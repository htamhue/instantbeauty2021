/* ---
  Docs: https://www.npmjs.com/package/mati-mix/
--- */
const mix = require('mati-mix');

mix.js([
    'assets-src/js/new-rules-table-popup.js',
], 'assets/js/new-rules-table-popup.js');

mix.sass(
    'assets-src/scss/new-rules-table-popup.scss'
, 'assets/css/new-rules-table-popup.css');

mix.mix.babelConfig({
    "presets": [
        "@babel/preset-env",
        "@babel/preset-react",
    ],
    "plugins": ["@babel/plugin-proposal-class-properties"]
});

