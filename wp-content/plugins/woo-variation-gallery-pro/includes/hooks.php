<?php
	
	defined( 'ABSPATH' ) or die( 'Keep Quit' );
	
	
	add_filter( 'woocommerce_get_sections_woo-variation-gallery', function () {
		return array(
			''          => esc_html__( 'General', 'woo-variation-gallery-pro' ),
			'configure' => esc_html__( 'Configuration', 'woo-variation-gallery-pro' ),
			'advanced'  => esc_html__( 'Advanced', 'woo-variation-gallery-pro' ),
			'license'   => esc_html__( 'License', 'woo-variation-gallery-pro' ),
			'tutorials' => esc_html__( 'Tutorials', 'woo-variation-gallery-pro' )
		);
	} );
	
	add_filter( 'woocommerce_get_settings_woo-variation-gallery', function ( $settings, $current_section ) {
		
		switch ( $current_section ):
			
			case 'configure':
				$settings = apply_filters( 'woo_variation_gallery_configure_settings', array(
					
					array(
						'name' => __( 'Gallery Configure', 'woo-variation-gallery-pro' ),
						'type' => 'title',
						'desc' => '',
						'id'   => 'woo_variation_gallery_configure_settings',
					),
					
					array(
						'title'   => esc_html__( 'Gallery Auto play', 'woo-variation-gallery-pro' ),
						'type'    => 'checkbox',
						'default' => 'no',
						'desc'    => esc_html__( 'Gallery Auto Slide / Auto Play', 'woo-variation-gallery-pro' ),
						'id'      => 'woo_variation_gallery_slider_autoplay'
					),
					
					array(
						'title'             => esc_html__( 'Gallery Auto Play Speed', 'woo-variation-gallery-pro' ),
						'type'              => 'number',
						'default'           => 5000,
						'css'               => 'width:70px;',
						'desc'              => esc_html__( 'Slider gallery autoplay speed. Default is 3000 means 3 seconds', 'woo-variation-gallery-pro' ),
						'id'                => 'woo_variation_gallery_slider_autoplay_speed',
						'custom_attributes' => array(
							'min'  => 500,
							'max'  => 10000,
							'step' => 500,
						),
					),
					
					array(
						'title'             => esc_html__( 'Gallery Slide / Fade Speed', 'woo-variation-gallery-pro' ),
						'type'              => 'number',
						'default'           => 300,
						'css'               => 'width:60px;',
						'desc'              => esc_html__( 'Gallery sliding speed. Default is 300 means 300 milliseconds', 'woo-variation-gallery-pro' ),
						'id'                => 'woo_variation_gallery_slide_speed',
						'custom_attributes' => array(
							'min'  => 100,
							'max'  => 1000,
							'step' => 100,
						),
					),
					
					
					array(
						'title'   => esc_html__( 'Fade Slide', 'woo-variation-gallery-pro' ),
						'type'    => 'checkbox',
						'default' => 'no',
						'desc'    => esc_html__( 'Gallery will change by fade not slide', 'woo-variation-gallery-pro' ),
						'id'      => 'woo_variation_gallery_slider_fade'
					),
					
					
					array(
						'title'   => esc_html__( 'Show Slider Arrow', 'woo-variation-gallery-pro' ),
						'type'    => 'checkbox',
						'default' => 'yes',
						'desc'    => esc_html__( 'Show Gallery Slider Arrow', 'woo-variation-gallery-pro' ),
						'id'      => 'woo_variation_gallery_slider_arrow'
					),
					
					array(
						'title'   => esc_html__( 'Enable Image Zoom', 'woo-variation-gallery-pro' ),
						'type'    => 'checkbox',
						'default' => 'yes',
						'desc'    => esc_html__( 'Enable Gallery Image Zoom', 'woo-variation-gallery-pro' ),
						'id'      => 'woo_variation_gallery_zoom'
					),
					
					array(
						'title'   => esc_html__( 'Enable Image Popup', 'woo-variation-gallery-pro' ),
						'type'    => 'checkbox',
						'default' => 'yes',
						'desc'    => esc_html__( 'Enable Gallery Image Popup', 'woo-variation-gallery-pro' ),
						'id'      => 'woo_variation_gallery_lightbox'
					),
					
					array(
						'title'   => esc_html__( 'Enable Thumbnail Slide', 'woo-variation-gallery-pro' ),
						'type'    => 'checkbox',
						'default' => 'yes',
						'desc'    => esc_html__( 'Enable Gallery Thumbnail Slide', 'woo-variation-gallery-pro' ),
						'id'      => 'woo_variation_gallery_thumbnail_slide'
					),
					
					array(
						'title'   => esc_html__( 'Show Thumbnail Arrow', 'woo-variation-gallery-pro' ),
						'type'    => 'checkbox',
						'default' => 'yes',
						'desc'    => esc_html__( 'Show Gallery Thumbnail Arrow', 'woo-variation-gallery-pro' ),
						'id'      => 'woo_variation_gallery_thumbnail_arrow'
					),
					
					array(
						'title'    => esc_html__( 'Zoom Icon Display Position', 'woo-variation-gallery-pro' ),
						'id'       => 'woo_variation_gallery_zoom_position',
						'default'  => 'top-right',
						//'type'     => 'radio',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'desc_tip' => esc_html__( 'Product Gallery Zoom Icon Display Position', 'woo-variation-gallery-pro' ),
						'options'  => array(
							'top-right'    => esc_html__( 'Top Right', 'woo-variation-gallery-pro' ),
							'top-left'     => esc_html__( 'Top Left', 'woo-variation-gallery-pro' ),
							'bottom-right' => esc_html__( 'Bottom Right', 'woo-variation-gallery-pro' ),
							'bottom-left'  => esc_html__( 'Bottom Left', 'woo-variation-gallery-pro' ),
						),
					),
					
					array(
						'title'    => esc_html__( 'Thumbnail Display Position', 'woo-variation-gallery-pro' ),
						'id'       => 'woo_variation_gallery_thumbnail_position',
						'default'  => 'bottom',
						//'type'     => 'radio',
						'type'     => 'select',
						'class'    => 'wc-enhanced-select',
						'desc_tip' => esc_html__( 'Product Gallery Thumbnail Display Position', 'woo-variation-gallery-pro' ),
						'options'  => array(
							'left'   => esc_html__( 'Left', 'woo-variation-gallery-pro' ),
							'right'  => esc_html__( 'Right', 'woo-variation-gallery-pro' ),
							'bottom' => esc_html__( 'Bottom', 'woo-variation-gallery-pro' ),
						),
					),
					
					
					array(
						'type' => 'sectionend',
						'id'   => 'woo_variation_gallery_configure_settings'
					),
				
				) );
				break;
			
			case 'license':
				$settings = apply_filters( 'woo_variation_gallery_license_settings', array(
					
					array(
						'name' => __( 'License', 'woo-variation-gallery-pro' ),
						'type' => 'title',
						'desc' => '',
						'id'   => 'woo_variation_gallery_license_options',
					),
					
					array(
						'title' => esc_html__( 'License key', 'woo-variation-gallery-pro' ),
						'type'  => 'text',
						'desc'  => '<br>' . __( 'Please add product license key and add your domain(s) on <a target="_blank" href="https://getwooplugins.com/my-account/downloads/">GetWooPlugins.com -> My Downloads</a> to get automatic update.', 'woo-variation-gallery-pro' ),
						'id'    => 'woo_variation_gallery_license'
					),
					
					
					array(
						'type' => 'sectionend',
						'id'   => 'woo_variation_gallery_license_options'
					),
				
				) );
				break;
		
		endswitch;
		
		return $settings;
		
	}, 10, 2 );
	
	add_filter( 'attachment_fields_to_edit', function ( $form_fields, $post ) {
		
		$form_fields[ 'woo_variation_gallery_media_title' ] = array(
			'tr' => sprintf( '<hr><h2>%s</h2>', __( 'Variation Gallery Video', 'woo-variation-gallery-pro' ) )
		);
		
		$form_fields[ 'woo_variation_gallery_media_video' ] = array(
			'label' => esc_html__( 'Video URL', 'woo-variation-gallery-pro' ),
			'input' => 'text',
			//'show_in_edit' => false,
			'value' => esc_url( get_post_meta( $post->ID, 'woo_variation_gallery_media_video', true ) )
		);
		
		$form_fields[ 'woo_variation_gallery_media_video_popup' ] = array(
			'label' => '',
			'input' => 'html',
			//'show_in_edit' => false,
			'html'  => '<a class="woo_variation_gallery_media_video_popup_link" href="#"><span class="dashicons dashicons-video-alt3"></span></a>',
		);
		
		$form_fields[ 'woo_variation_gallery_media_video_width' ] = array(
			'label' => esc_html__( 'Width', 'woo-variation-gallery-pro' ),
			'input' => 'text',
			//'show_in_edit' => false,
			'value' => absint( get_post_meta( $post->ID, 'woo_variation_gallery_media_video_width', true ) ),
			'helps' => esc_html__( 'Actual Video Width or Width Ratio. Empty for default', 'woo-variation-gallery-pro' )
		);
		
		$form_fields[ 'woo_variation_gallery_media_video_height' ] = array(
			'label' => esc_html__( 'Height', 'woo-variation-gallery-pro' ),
			'input' => 'text',
			//'show_in_edit' => false,
			'value' => absint( get_post_meta( $post->ID, 'woo_variation_gallery_media_video_height', true ) ),
			'helps' => esc_html__( 'Actual Video Height or Height Ratio. Empty for default', 'woo-variation-gallery-pro' )
		);
		
		return $form_fields;
	}, 10, 2 );
	
	add_filter( 'attachment_fields_to_save', function ( $post, $attachment ) {
		
		if ( isset( $attachment[ 'woo_variation_gallery_media_video' ] ) ) {
			update_post_meta( $post[ 'ID' ], 'woo_variation_gallery_media_video', esc_url( trim( $attachment[ 'woo_variation_gallery_media_video' ] ) ) );
		}
		
		if ( isset( $attachment[ 'woo_variation_gallery_media_video_width' ] ) ) {
			update_post_meta( $post[ 'ID' ], 'woo_variation_gallery_media_video_width', absint( trim( $attachment[ 'woo_variation_gallery_media_video_width' ] ) ) );
		}
		
		if ( isset( $attachment[ 'woo_variation_gallery_media_video_height' ] ) ) {
			update_post_meta( $post[ 'ID' ], 'woo_variation_gallery_media_video_height', absint( trim( $attachment[ 'woo_variation_gallery_media_video_height' ] ) ) );
		}
		
		return $post;
	}, 10, 2 );
	
	add_filter( 'wp_prepare_attachment_for_js', function ( $response, $attachment, $meta ) {
		
		$id        = absint( $attachment->ID );
		$has_video = trim( get_post_meta( $id, 'woo_variation_gallery_media_video', true ) );
		
		$response[ 'woo_variation_gallery_video' ] = $has_video;
		
		return $response;
	}, 10, 3 );
	
	add_filter( 'woo_variation_gallery_admin_template_js', function () {
		require_once 'admin-template-js.php';
	} );
	
	add_filter( 'woo_variation_gallery_slider_template_js', function () {
		require_once 'slider-template-js.php';
	} );
	
	add_filter( 'woo_variation_gallery_thumbnail_template_js', function () {
		require_once 'thumbnail-template-js.php';
	} );
	
	add_filter( 'woo_variation_gallery_slider_js_options', function ( $options ) {
		
		if ( 'yes' === get_option( 'woo_variation_gallery_slider_arrow', 'yes' ) ) {
			$options[ 'arrows' ] = true;
		}
		
		if ( 'yes' === get_option( 'woo_variation_gallery_thumbnail_slide', 'yes' ) ) {
			$options[ 'asNavFor' ] = '.woo-variation-gallery-thumbnail-slider';
		}
		
		$options[ 'prevArrow' ] = '<i class="wvg-slider-prev-arrow dashicons dashicons-arrow-left-alt2"></i>';
		$options[ 'nextArrow' ] = '<i class="wvg-slider-next-arrow dashicons dashicons-arrow-right-alt2"></i>';
		$options[ 'speed' ]     = absint( get_option( 'woo_variation_gallery_slide_speed', 300 ) );
		
		
		if ( 'yes' === get_option( 'woo_variation_gallery_slider_autoplay', 'no' ) ) {
			$options[ 'autoplay' ]      = true;
			$options[ 'autoplaySpeed' ] = absint( get_option( 'woo_variation_gallery_slider_autoplay_speed', 5000 ) );
		}
		
		
		if ( 'yes' === get_option( 'woo_variation_gallery_slider_fade', 'no' ) ) {
			$options[ 'fade' ] = true;
		}
		
		return $options;
	} );
	
	add_filter( 'woo_variation_gallery_thumbnail_slider_js_options', function ( $options ) {
		
		if ( 'yes' === get_option( 'woo_variation_gallery_thumbnail_arrow', 'yes' ) ) {
			$options[ 'arrows' ] = true;
		}
		
		$options[ 'prevArrow' ] = '<i class="wvg-thumbnail-prev-arrow dashicons dashicons-arrow-left-alt2"></i>';
		$options[ 'nextArrow' ] = '<i class="wvg-thumbnail-next-arrow dashicons dashicons-arrow-right-alt2"></i>';
		
		$thumbnail_position = get_option( 'woo_variation_gallery_thumbnail_position', 'bottom' );
		
		if ( in_array( $thumbnail_position, array( 'left', 'right' ) ) ) {
			$options[ 'vertical' ] = true;
		}
		
		$options[ 'responsive' ] = array(
			array(
				'breakpoint' => 768,
				'settings'   => array(
					'vertical' => false
				),
			),
			
			array(
				'breakpoint' => 480,
				'settings'   => array(
					'vertical' => false
				),
			)
		);
		
		return $options;
	} );
	
	add_filter( 'woo_variation_gallery_js_options', function ( $options ) {
		
		if ( 'yes' === get_option( 'woo_variation_gallery_thumbnail_slide', 'yes' ) ) {
			$options[ 'enable_thumbnail_slide' ] = true;
		}
		
		$thumbnail_position                           = get_option( 'woo_variation_gallery_thumbnail_position', 'bottom' );
		$options[ 'is_vertical' ]                     = in_array( $thumbnail_position, array( 'left', 'right' ) );
		$options[ 'thumbnail_position' ]              = trim( $thumbnail_position );
		$options[ 'thumbnail_position_class_prefix' ] = 'woo-variation-gallery-thumbnail-position-';
		
		return $options;
	} );
	
	add_filter( 'woo_variation_gallery_product_image_classes', function ( $classes ) {
		
		if ( 'yes' === get_option( 'woo_variation_gallery_thumbnail_slide', 'yes' ) ) {
			$classes[] = 'woo-variation-gallery-enabled-thumbnail-slider';
		}
		
		return $classes;
	} );
	
	add_filter( 'woo_variation_gallery_get_image_props', function ( $props, $attachment_id ) {
		
		
		$has_video = trim( get_post_meta( $attachment_id, 'woo_variation_gallery_media_video', true ) );
		$type      = wp_check_filetype( $has_video );
		
		$video_width  = absint( trim( get_post_meta( $attachment_id, 'woo_variation_gallery_media_video_width', true ) ) );
		$video_height = absint( trim( get_post_meta( $attachment_id, 'woo_variation_gallery_media_video_height', true ) ) );
		
		$props[ 'video_link' ] = $has_video;
		// $props[ 'video_thumbnail_src' ] = woo_variation_gallery()->images_uri( 'play-button.svg' );
		
		if ( ! empty( $has_video ) ) {
			
			if ( ! empty( $type[ 'type' ] ) ) {
				$props[ 'video_embed_type' ] = 'video';
			} else {
				$props[ 'video_embed_type' ] = 'iframe';
				$props[ 'video_embed_url' ]  = wvg_get_simple_embed_url( $has_video );
			}
			
			$props[ 'video_width' ]  = $video_width ? $video_width : 1;
			$props[ 'video_height' ] = $video_height ? $video_height : 1;
			$props[ 'video_ratio' ]  = ( $props[ 'video_height' ] / $props[ 'video_width' ] ) * 100;
		}
		
		return $props;
		
	}, 10, 2 );
	
	add_filter( 'wvg_gallery_image_html_class', function ( $class, $attachment_id, $image ) {
		
		if ( ! empty( $image[ 'video_link' ] ) ) {
			array_push( $class, 'wvg-gallery-video-slider' );
		}
		
		return $class;
	}, 10, 3 );
	
	add_filter( 'woo_variation_gallery_thumbnail_image_html_class', function ( $class, $attachment_id, $image ) {
		
		if ( ! empty( $image[ 'video_link' ] ) ) {
			array_push( $class, 'wvg-gallery-video-thumbnail' );
		}
		
		return $class;
	}, 10, 3 );
	
	add_filter( 'woo_variation_gallery_image_inner_html', function ( $inner_html, $image, $template, $attachment_id, $options ) {
		
		if ( $options[ 'has_only_thumbnail' ] ) {
			return $inner_html;
		}
		
		if ( $image[ 'video_link' ] && $image[ 'video_embed_type' ] === 'iframe' ) {
			$template   = '<div class="wvg-single-gallery-iframe-container" style="padding-bottom: %d%%"><iframe src="%s" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>';
			$inner_html = sprintf( $template, $image[ 'video_ratio' ], $image[ 'video_embed_url' ] );
		}
		
		if ( $image[ 'video_link' ] && $image[ 'video_embed_type' ] === 'video' ) {
			$template   = '<div class="wvg-single-gallery-video-container" style="padding-bottom: %d%%"><video preload="auto" controls controlsList="nodownload" src="%s"></video></div>';
			$inner_html = sprintf( $template, $image[ 'video_ratio' ], $image[ 'video_link' ] );
		}
		
		return $inner_html;
	}, 10, 5 );