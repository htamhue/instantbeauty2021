/*!
 * Additional Variation Images Gallery for WooCommerce - Pro v1.1.23 
 * 
 * Author: Emran Ahmed ( emran.bd.08@gmail.com ) 
 * Date: 3/16/2020, 5:19:57 PM
 * Released under the GPLv3 license.
 */
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(1);
__webpack_require__(3);
module.exports = __webpack_require__(4);


/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

jQuery(function ($) {
    Promise.resolve().then(function () {
        return __webpack_require__(2);
    }).then(function () {
        $(document).on('woo_variation_gallery_init', function () {
            $('.woo-variation-gallery-wrapper').WooVariationGalleryPro();
        });
    });
}); // end of jquery main wrapper

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// ================================================================
// WooCommerce Variation Gallery
/*global wc_add_to_cart_variation_params, woo_variation_gallery_options, _ */
// ================================================================

var WooVariationGalleryPro = function ($) {

    var Default = {};

    var WooVariationGalleryPro = function () {
        function WooVariationGalleryPro(element, config) {
            _classCallCheck(this, WooVariationGalleryPro);

            // Assign
            this._el = element;
            this._element = $(element);
            this._config = $.extend({}, Default, config);

            this.$product = this._element.closest('.product');
            this.$variations_form = this.$product.find('.variations_form');
            this.$target = this._element.parent();
            this.$slider = $('.woo-variation-gallery-slider', this._element);
            this.$thumbnail = $('.woo-variation-gallery-thumbnail-slider', this._element);

            // Temp variable
            this.is_vertical = !!woo_variation_gallery_options.is_vertical;

            // Call
            this.init();

            this._element.data('woo_variation_gallery_pro', this);
            $(document).trigger('woo_variation_gallery_pro_init', [this]);
        }

        _createClass(WooVariationGalleryPro, [{
            key: 'init',
            value: function init() {
                var _this = this;

                this._element.on('woo_variation_gallery_slider_slick_init', function (event, gallery) {

                    if (woo_variation_gallery_options.is_vertical) {

                        //$(window).off('resize.wvg');

                        $(window).on('resize', _this.enableThumbnailPositionDebounce());
                        //$(window).on('resize', this.thumbnailHeightDebounce());

                        //this.$slider.on('setPosition', this.enableThumbnailPositionDebounce());
                        _this.$slider.on('setPosition', _this.thumbnailHeightDebounce());

                        _this.$slider.on('afterChange', function () {
                            _this.thumbnailHeight();
                        });
                    }

                    if (woo_variation_gallery_options.enable_thumbnail_slide) {

                        var thumbnails = _this.$thumbnail.find('.wvg-gallery-thumbnail-image').length;

                        if (parseInt(woo_variation_gallery_options.gallery_thumbnails_columns) < thumbnails) {
                            _this.$thumbnail.find('.wvg-gallery-thumbnail-image').removeClass('current-thumbnail');
                            _this.initThumbnailSlick();
                        } else {
                            _this.$slider.slick('slickSetOption', 'asNavFor', null, false);
                        }
                    }
                });

                this._element.on('woo_variation_gallery_slider_slick_init', function (event, gallery) {
                    if (_this.$slider.hasClass('slick-initialized')) {
                        // this.$slider.slick('setPosition');
                    }
                });

                this._element.on('woo_variation_gallery_slick_destroy', function (event, gallery) {
                    if (_this.$thumbnail.hasClass('slick-initialized')) {
                        _this.$thumbnail.slick('unslick');
                    }
                });
            }
        }, {
            key: 'initThumbnailSlick',
            value: function initThumbnailSlick() {
                var _this2 = this;

                if (this.$thumbnail.hasClass('slick-initialized')) {
                    this.$thumbnail.slick('unslick');
                }

                this.$thumbnail.off('init');

                this.$thumbnail.on('init', function () {}).slick();

                _.delay(function () {
                    _this2._element.trigger('woo_variation_gallery_thumbnail_slick_init', [_this2]);
                }, 1);
            }
        }, {
            key: 'thumbnailHeight',
            value: function thumbnailHeight() {

                //console.log('thumbnailHeight...')
                if (this.is_vertical) {
                    if (this.$slider.slick('getSlick').$slides.length > 1) {
                        this.$thumbnail.height(this.$slider.height());
                    } else {
                        this.$thumbnail.height(0);
                    }
                } else {
                    this.$thumbnail.height('auto');
                }

                if (this.$thumbnail.hasClass('slick-initialized')) {
                    this.$thumbnail.slick('setPosition');
                }
            }
        }, {
            key: 'thumbnailHeightDebounce',
            value: function thumbnailHeightDebounce(event) {
                var _this3 = this;

                return _.debounce(function () {
                    _this3.thumbnailHeight();
                }, 401);
            }
        }, {
            key: 'enableThumbnailPosition',
            value: function enableThumbnailPosition() {

                if (!woo_variation_gallery_options.is_mobile) {
                    //    return;
                }

                if (woo_variation_gallery_options.is_vertical) {
                    //console.log('enableThumbnailPosition...')
                    if (window.matchMedia("(max-width: 768px)").matches || window.matchMedia("(max-width: 480px)").matches) {

                        this.is_vertical = false;

                        this._element.removeClass(woo_variation_gallery_options.thumbnail_position_class_prefix + 'left ' + woo_variation_gallery_options.thumbnail_position_class_prefix + 'right ' + woo_variation_gallery_options.thumbnail_position_class_prefix + 'bottom');
                        this._element.addClass(woo_variation_gallery_options.thumbnail_position_class_prefix + 'bottom');

                        this.$slider.slick('setPosition');
                    } else {

                        this.is_vertical = true;

                        this._element.removeClass(woo_variation_gallery_options.thumbnail_position_class_prefix + 'left ' + woo_variation_gallery_options.thumbnail_position_class_prefix + 'right ' + woo_variation_gallery_options.thumbnail_position_class_prefix + 'bottom');
                        this._element.addClass('' + woo_variation_gallery_options.thumbnail_position_class_prefix + woo_variation_gallery_options.thumbnail_position);

                        this.$slider.slick('setPosition');
                    }
                }
            }
        }, {
            key: 'enableThumbnailPositionDebounce',
            value: function enableThumbnailPositionDebounce(event) {
                var _this4 = this;

                return _.debounce(function () {
                    _this4.enableThumbnailPosition();
                }, 400);
            }
        }], [{
            key: '_jQueryInterface',
            value: function _jQueryInterface(config) {
                return this.each(function () {
                    new WooVariationGalleryPro(this, config);
                });
            }
        }]);

        return WooVariationGalleryPro;
    }();

    /**
     * ------------------------------------------------------------------------
     * jQuery
     * ------------------------------------------------------------------------
     */

    $.fn['WooVariationGalleryPro'] = WooVariationGalleryPro._jQueryInterface;
    $.fn['WooVariationGalleryPro'].Constructor = WooVariationGalleryPro;
    $.fn['WooVariationGalleryPro'].noConflict = function () {
        $.fn['WooVariationGalleryPro'] = $.fn['WooVariationGalleryPro'];
        return WooVariationGalleryPro._jQueryInterface;
    };

    return WooVariationGalleryPro;
}(jQuery);

/* harmony default export */ __webpack_exports__["default"] = (WooVariationGalleryPro);

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);