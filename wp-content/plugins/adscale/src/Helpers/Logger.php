<?php

namespace AdScale\Helpers;


class Logger {
	
	
	protected static $config = [];
	
	
	
	
	public static function _log(
		$var,
		$desc = ' >> ',
		$log_file_destination = null,
		$clear_log = false,
		$no_print_r = false
	) {
		
		if ( ! self::isLoggerEnabled() ) {
			return;
		}
		
		if ( ! $log_file_destination ) {
			return;
		}
		
		$dirname = dirname( $log_file_destination );
		
		// try to make directory if not exists
		if ( ! self::resolveDir( $dirname ) ) {
			return;
		}
		
		self::resolveDenyHtaccess( $dirname );
		
		if ( $clear_log ) {
			file_put_contents( $log_file_destination, '' );
		}
		
		if ( self::isLoggerTimeInMS() ) {
			$time_mark = '[' . self::getMicroTime() . '] ';
		} else {
			$time_mark = '[' . date( 'Y-m-d H:i:s' ) . '] ';
		}
		
		if ( $no_print_r ) {
			if ( is_numeric( $var ) || is_string( $var ) || is_bool( $var ) ) {
				error_log( $time_mark . $desc . ' ' . $var . PHP_EOL, 3, $log_file_destination );
			} elseif ( is_object( $var ) ) {
				error_log( $time_mark . $desc . ' Object:' . get_class( $var ) . PHP_EOL, 3, $log_file_destination );
			} elseif ( is_array( $var ) ) {
				error_log( $time_mark . $desc . ' Array' . PHP_EOL, 3, $log_file_destination );
			} elseif ( is_resource( $var ) ) {
				error_log( $time_mark . $desc . ' Resource' . PHP_EOL, 3, $log_file_destination );
			} else {
				error_log( $time_mark . $desc . ' Something unprintable' . PHP_EOL, 3, $log_file_destination );
			}
		} else {
			error_log( $time_mark . $desc . ' ' . print_r( $var, true ) . PHP_EOL, 3, $log_file_destination );
		}
		
	}
	
	
	
	
	public static function log( $var, $desc = ' ', $name = 'default', $separate = false, $no_print_r = false ) {
		
		if ( ! self::isLoggerEnabled() ) {
			return;
		}
		
		$date = $separate ? date( 'Y-m-d_H' ) : date( 'Y-m-d' );
		$dir  = self::getDir();
		
		$log_file_destination = trailingslashit( $dir ) . $name . '_' . $date . '.log';
		self::_log( $var, $desc, $log_file_destination, false, $no_print_r );
		
	}
	
	
	
	// ==============================================================================
	
	
	
	public static function setConfig( array $config ) {
		return self::$config = $config;
	}
	
	
	
	
	public static function getConfig() {
		return self::$config;
	}
	
	
	
	
	public static function isLoggerEnabled() {
		return isset( self::$config['enabled'] ) ? self::$config['enabled'] : false;
	}
	
	
	
	
	public static function isLoggerTimeInMS() {
		return isset( self::$config['time_in_ms'] ) ? self::$config['time_in_ms'] : true;
	}
	
	
	
	
	public static function getDir() {
		return isset( self::$config['dir'] ) ? self::$config['dir'] : get_stylesheet_directory();
	}
	
	
	
	
	public static function isNeedDenyHtaccess() {
		return isset( self::$config['need_deny_htaccess'] ) ? self::$config['need_deny_htaccess'] : false;
	}
	
	
	
	
	public static function getMicroTime() {
		$t     = microtime( true );
		$micro = sprintf( '%06d', ( $t - floor( $t ) ) * 1000000 );
		$d     = null;
		try {
			$d = new \DateTime( date( 'Y-m-d H:i:s.' . $micro, $t ) );
		} catch ( \Exception $e ) {
			
		}
		
		return $d ? $d->format( 'Y-m-d H:i:s.u' ) : ''; //note "u" is microseconds (1 seconds = 1000000 µs).
	}
	
	
	
	
	public static function resolveDir( $dirname ) {
		
		$dir_exists = is_dir( $dirname );
		
		if ( ! $dir_exists ) {
			$dir_exists = @mkdir( $dirname, 0775, true );
		}
		
		return $dir_exists;
	}
	
	
	
	
	public static function resolveDenyHtaccess( $dirname ) {
		
		if ( ! self::isNeedDenyHtaccess() ) {
			return null;
		}
		
		if ( ! is_dir( $dirname ) ) {
			return null;
		}
		
		$htaccess_path = trailingslashit( $dirname ) . '.htaccess';
		
		if ( is_file( $htaccess_path ) ) {
			return null;
		}
		
		$content = <<<EOT
order deny,allow
deny from all
EOT;
		
		return file_put_contents( $htaccess_path, $content, LOCK_EX );
	}
	
	
}