<?php

namespace AdScale\Helpers;

use AdScale\App;
use AdScale\API\API_Manager;

if ( ! class_exists( Helper::class ) ) {
	
	class Helper {
		
		
		public static function is_product_page() {
			return function_exists( 'is_product' ) && is_product();
		}
		
		
		
		
		public static function is_order_received_endpoint() {
			return function_exists( 'is_wc_endpoint_url' ) && is_wc_endpoint_url( 'order-received' );
		}
		
		
		
		
		public static function is_order_received_page() {
			return function_exists( 'is_order_received_page' ) && is_order_received_page();
		}
		
		
		
		
		public static function get_product_id() {
			
			$product = function_exists( 'wc_get_product' ) ? wc_get_product() : 0;
			
			if ( ! is_object( $product ) || ! method_exists( $product, 'get_id' ) || ! $product_id = $product->get_id() ) {
				return 0;
			}
			
			return $product_id;
		}
		
		
		
		
		public static function get_product_value() {
			
			$product = function_exists( 'wc_get_product' ) ? wc_get_product() : 0;
			
			if ( ! is_object( $product ) || ! method_exists( $product, 'get_price' ) ) {
				return false;
			}
			
			return $product->get_price();
		}
		
		
		
		
		public static function get_order() {
			$order = function_exists( 'wc_get_order' ) ? wc_get_order() : false;
			
			if ( ! is_object( $order ) || ! method_exists( $order, 'get_id' ) || ! $order_id = $order->get_id() ) {
				return self::get_order_thankyou();
			}
			
			return $order;
		}
		
		
		
		
		public static function get_order_thankyou() {
			// Only in thankyou "Order-received" page
			if ( ! is_wc_endpoint_url( 'order-received' ) ) {
				return false;
			}
			
			global $wp;
			
			// Get the order ID
			$order_id = absint( $wp->query_vars['order-received'] );
			
			if ( empty( $order_id ) ) {
				return false;
			}
			
			return wc_get_order( $order_id );
		}
		
		
		
		
		public static function get_order_by_get_params() {
			// check if the order ID exists and if is set the order key
			if ( ! isset( $_GET['order'], $_GET['key'] ) ) {
				return false;
			}
			
			$order_id = (int) $_GET['order'];
			//get order object 
			$order = wc_get_order( $order_id );
			
			//order exists check the order key passed is isset and if it is the same of the order
			$order_key = $order->get_order_key();
			if ( isset( $_GET['key'] ) && $_GET['key'] !== $order_key ) {
				return false;
			}
			
			return $order;
		}
		
		
		
		
		/**
		 * @param $_order mixed; order id | order object WP_Post | order object instanceof WC_Abstract_Order
		 *
		 * @return bool|float
		 */
		public static function get_order_value( $_order ) {
			
			$order = function_exists( 'wc_get_order' ) ? wc_get_order( $_order ) : false;
			
			if ( ! is_object( $order ) || ! method_exists( $order, 'get_total' ) ) {
				return false;
			}
			
			return $order->get_total();
		}
		
		
		
		
		public static function resolve_value_for_inline_js( $value ) {
			$value = (string) $value;
			$value = $value === '' ? '""' : $value;
			$value = is_numeric( $value ) ? $value : "'{$value}'";
			
			return $value;
		}
		
		
		
		
		public static function getShopDomainComputed() {
			$shop_domain = parse_url( home_url(), PHP_URL_HOST );
			$shop_path   = parse_url( home_url(), PHP_URL_PATH );
			$prefix      = 'www.';
			if ( strpos( $shop_domain, $prefix ) === 0 ) {
				$shop_domain = substr( $shop_domain, strlen( $prefix ) );
			}
			
			return str_replace( '/', '_', $shop_domain . $shop_path );
		}
		
		
		
		
		public static function getShopDomainFromOption() {
			return get_option( 'adscale__shop_domain', '' );
		}
		
		
		
		
		public static function getShopDomain() {
			$from_option = self::getShopDomainFromOption();
			$computed    = self::getShopDomainComputed();  // bc
			
			return $from_option ? $from_option : $computed;
		}
		
		
		
		
		public static function saveComputedShopDomain() {
			return update_option( 'adscale__shop_domain', self::getShopDomainComputed() );
		}
		
		
		
		
		public static function getProxyEndpoint() {
			return get_option( 'adscale__proxy_endpoint', '' );
		}
		
		
		
		
		public static function saveDefaultProxyEndpoint() {
			$proxy_from_config = Helper::getConfigSetting( 'adscale_proxy_url_base', '' );
			
			return update_option( 'adscale__proxy_endpoint', $proxy_from_config );
		}
		
		
		
		
		public static function getGSVCode() {
			$value    = API_Manager::get_saved_api_gsv_code();
			$bc_value = get_option( 'adscale__gsv_from_api', '' );
			
			return $value ? $value : $bc_value;
		}
		
		
		
		
		public static function getConfigSetting( $name, $default, $direct = false ) {
			$parts = explode( '/', $name );
			
			$config = $direct
				? apply_filters( 'adscale/config', require ADSCALE_PLUGIN_DIR . '/src/config/config.php' )
				: App::instance()->getConfig();
			
			if ( ! isset( $config[ $parts[0] ] ) ) {
				return $default;
			}
			
			$value = $config[ array_shift( $parts ) ];
			
			foreach ( $parts as $part ) {
				if ( is_array( $value ) && isset( $value[ $part ] ) ) {
					$value = $value[ $part ];
				} else {
					return $default;
				}
			}
			
			return $value;
		}
		
		
		
		
		public static function generateRandString( $length = 32 ) {
			$chars          = '123456789ABCDEFGHIJKLMNPQRSTUVWXYZ';
			$chars_length   = strlen( $chars );
			$generatedValue = '';
			
			for ( $i = 1; $i <= $length; ++ $i ) {
				$generatedValue .= substr( $chars, (int) floor( self::randomFloat( 0, 1 ) * ( $chars_length - 1 ) ), 1 );
			}
			
			return $generatedValue;
		}
		
		
		
		
		public static function randomFloat( $min = 0, $max = 1 ) {
			return $min + mt_rand() / mt_getrandmax() * ( $max - $min );
		}
		
		
		
		
		public static function generateRandHash( $limit = 32 ) {
			if ( ! function_exists( 'openssl_random_pseudo_bytes' ) ) {
				return self::generateRandString( $limit );
			}
			
			$bytes = openssl_random_pseudo_bytes( $limit );
			$hex   = strtoupper( bin2hex( $bytes ) );
			
			return substr( $hex, 0, $limit );
		}
		
		
		
		
		public static function getAccessKey() {
			return @hex2bin( self::getPostBodyDataValue( 'key' ) );
		}
		
		
		
		
		public static function getPostBodyDataValue( $key ) {
			$data = json_decode( file_get_contents( 'php://input' ), false );
			
			return isset( $data->$key ) ? $data->$key : null;
		}
		
		
		
		
		public static function delToEnsureRewriteRule( $rewrite_rule_regex ) {
			$rewrite_rules = get_option( 'rewrite_rules' );
			if ( is_array( $rewrite_rules ) && ! array_key_exists( $rewrite_rule_regex, $rewrite_rules ) ) {
				delete_option( 'rewrite_rules' );
			}
		}
		
		
		
		public static function getResolvedActivationEmail() {
			$infoFileEmail = self::getInfoFileEmail();
			
			return $infoFileEmail ? $infoFileEmail : self::getSiteEmail();
		}
		
		
		public static function getSiteEmail() {
			return get_option( 'admin_email' );
		}
		
		
		public static function getInfoFileEmail() {
			return self::getInfoFileValue( 'email_on_download' );
		}
		
		
		public static function getInfoFileValue( $key ) {
			$data = self::getInfoFileData();
			
			return isset( $data[ $key ] ) ? $data[ $key ] : null;
		}
		
		
		public static function getInfoFileData() {
			$infoFilePath = ADSCALE_PLUGIN_DIR . '/info.php';
			
			if ( is_file( $infoFilePath ) ) {
				return include $infoFilePath;
			}
			
			return [];
		}
		
		
		
		/**
		 * @param string $response
		 *
		 * @return string
		 */
		public static function formatResponse( $response ) {
			return "ADSCALE_START{$response}ADSCALE_END";
		}
		
		
		
		/**
		 * Send a JSON response, indicating success.
		 *
		 * @param mixed $data Data to encode as JSON, then print and die.
		 * @param int $statusCode
		 * @param string $statusDesc
		 */
		public static function sendResponseJsonSuccess( $data = null, $statusCode = 200, $statusDesc = '' ) {
			self::sendResponseJson( $data, $statusCode, $statusDesc );
		}
		
		
		
		/**
		 * Send a JSON response, indicating failure.
		 *
		 * @param mixed $data Data to encode as JSON, then print and die.
		 * @param int $statusCode The HTTP status code to output.
		 * @param string $statusDesc
		 */
		public static function sendResponseJsonError( $data = null, $statusCode = 500, $statusDesc = '' ) {
			self::sendResponseJson( $data, $statusCode, $statusDesc );
		}
		
		
		
		public static function sendResponseJson( $response, $statusCode = null, $statusDesc = '' ) {
			self::sendResponse( json_encode( $response ), $statusCode, $statusDesc, 'application/json' );
		}
		
		
		
		/**
		 * Send a Formatted response, indicating success.
		 *
		 * @param mixed $data Data to encode as JSON, then print and die.
		 * @param int $statusCode
		 * @param string $statusDesc
		 */
		public static function sendResponseFormattedSuccess( $data = null, $statusCode = 200, $statusDesc = '' ) {
			self::sendResponseFormatted( $data, $statusCode, $statusDesc );
		}
		
		
		
		/**
		 * Send a Formatted response, indicating failure.
		 *
		 * @param mixed $data Data to encode as JSON, then print and die.
		 * @param int $statusCode The HTTP status code to output.
		 * @param string $statusDesc
		 */
		public static function sendResponseFormattedError( $data = null, $statusCode = 500, $statusDesc = '' ) {
			self::sendResponseFormatted( $data, $statusCode, $statusDesc );
		}
		
		
		
		public static function sendResponseFormatted( $response, $statusCode = null, $statusDesc = '' ) {
			self::sendResponse( self::formatResponse( json_encode( $response ) ), $statusCode, $statusDesc, 'text/plain' );
		}
		
		
		
		public static function sendResponse( $response, $statusCode = null, $statusDesc = '', $contentType = 'text/plain' ) {
			if ( ! headers_sent() ) {
				if ( null !== $statusCode ) {
					\remove_all_filters( 'status_header', false );
					\status_header( $statusCode, $statusDesc );
				}
				if ( is_string( $contentType ) && $contentType ) {
					header( "Content-Type: {$contentType}" );
				}
			}
			echo $response;
			die();
		}
		
		
		
		/**
		 * Set a cookie - wrapper for setcookie using WP constants.
		 *
		 * @param string $name Name of the cookie being set.
		 * @param string $value Value of the cookie.
		 * @param integer $expire Expiry of the cookie.
		 * @param bool $secure Whether the cookie should be served only over https.
		 * @param bool $httponly Whether the cookie is only accessible over HTTP, not scripting languages like JavaScript.
		 */
		public static function setCookie( $name, $value, $expire = 0, $secure = false, $httponly = false ) {
			if ( ! headers_sent() ) {
				setcookie( $name, $value, $expire, COOKIEPATH ? COOKIEPATH : '/', COOKIE_DOMAIN, $secure, $httponly );
				//Logger::log( "name [{$name}], value [{$value}]", 'setCookie: success: ', 'cookie' );
			} else {
				headers_sent( $file, $line );
				Logger::log( "{$name} cookie cannot be set - headers already sent by {$file} on line {$line}", 'setCookie: failed: ', 'cookie' );
			}
		}
		
		
		
		
		public static function getArrayValue( $array, $key ) {
			return isset( $array[ $key ] ) ? $array[ $key ] : null;
		}
		
		
		
		
		public static function nocacheHeaders() {
			if ( function_exists( 'wc_nocache_headers' ) ) {
				wc_nocache_headers();
			} else {
				nocache_headers();
			}
		}
		
		
		
		
		public static function getAdscaleLinkId() {
			return self::getArrayValue( $_COOKIE, 'adscale_link_id' );
		}
		
		
		
		
		public static function getWcPermalinkStructure() {
			$saved_permalinks = (array) get_option( 'woocommerce_permalinks', array() );
			$permalinks       = wp_parse_args(
				array_filter( $saved_permalinks ),
				array(
					'product_base'           => _x( 'product', 'slug', 'woocommerce' ),
					'category_base'          => _x( 'product-category', 'slug', 'woocommerce' ),
					'tag_base'               => _x( 'product-tag', 'slug', 'woocommerce' ),
					'attribute_base'         => '',
					'use_verbose_page_rules' => false,
				)
			);
			
			$permalinks['product_rewrite_slug']   = untrailingslashit( $permalinks['product_base'] );
			$permalinks['category_rewrite_slug']  = untrailingslashit( $permalinks['category_base'] );
			$permalinks['tag_rewrite_slug']       = untrailingslashit( $permalinks['tag_base'] );
			$permalinks['attribute_rewrite_slug'] = untrailingslashit( $permalinks['attribute_base'] );
			
			return $permalinks;
		}
		
		
		
		
		public static function saveDefaultConsiderPendingOrders() {
			$default_from_config = Helper::getConfigSetting( 'default_consider_pending_orders', true );
			
			return update_option( 'adscale__consider_pending_orders', $default_from_config );
		}
		
		
		
		
		public static function isConsiderPendingOrders() {
			return filter_var( get_option( 'adscale__consider_pending_orders', false ), FILTER_VALIDATE_BOOLEAN );
		}
		
		
		
		
		public static function validateSecKey( $key ) {
			
			if ( ! class_exists( '\Adscale_Crypt_RSA' ) ) {
				$libPath = ADSCALE_PLUGIN_DIR . '/lib/customized/phpseclib';
				set_include_path( get_include_path() . PATH_SEPARATOR . $libPath );
				require_once 'AdscaleCrypt/RSA.php';
			}
			
			$rsa = new \Adscale_Crypt_RSA();
			$rsa->setSignatureMode( ADSCALE_CRYPT_RSA_SIGNATURE_PKCS1 );
			$rsa->setEncryptionMode( ADSCALE_CRYPT_RSA_ENCRYPTION_PKCS1 );
			
			$rsa->loadKey( "-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCBMUfDGyQbPrNjBpZVkQ6Kabt4fo9W0fRlA7XKdbo2eg9P8i+TfBfeYBd6hlu/DlMnDL8AdElXKakNONT5B4y1/M15cBySmn1gDPLYs/u9rZrjmGLNZX/4sOomISUFihnDl2+3hGMrrGRQyuW78kY/ZJBpdqqybAJolJTe1J4R2wIDAQAB\n-----END PUBLIC KEY-----" );
			
			//return $key === '1111' || $key === hex2bin('1111'); // for testing
			
			return @$rsa->verify( self::getShopDomain(), $key );
		}
		
		
	}
	
} // class_exists