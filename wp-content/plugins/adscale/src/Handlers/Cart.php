<?php

namespace AdScale\Handlers;

use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;

class Cart {
	
	
	public static function ajaxGetCart() {
		$cart = self::getCartSimlified();
		
		if ( is_array( $cart ) ) {
			//Logger::log( $cart, 'ajaxGetCart : success : ', 'ajax_Cart' );
			
			status_header( 200 );  // 200 - OK
			header( 'Content-Type: application/json' );
			
			echo json_encode( $cart );
			die();
		}
		
		Logger::log( $cart, 'ajaxGetCart : failed : ', 'ajax_Cart' );
		
		wp_send_json_error( [ 'message' => 'ajaxGetCart : failed', 'request_params' => $_POST ] );
	}
	
	
	
	
	public static function getCartSimlified() {
		$cart = WC()->cart->get_cart();
		
		$products = [];
		
		if ( is_array( $cart ) && $cart ) {
			foreach ( $cart as $cart_item_key => $cart_item ) {
				$product               = [];
				$product['product_id'] = $cart_item['product_id'];
				$product['amount']     = $cart_item['quantity'];
				
				$Product = $cart_item['data'];
				if ( is_object( $Product ) && method_exists( $Product, 'get_price' ) ) {
					$product['price'] = $Product->get_price();
				} else {
					$product['price'] = null;
				}
				
				$products[] = $product;
			}
		}
		
		return [ 'products' => $products ];
	}
}	