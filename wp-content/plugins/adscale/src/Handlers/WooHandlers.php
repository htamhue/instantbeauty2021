<?php

namespace AdScale\Handlers;

use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;

if ( ! class_exists( WooHandlers::class ) ) {
	
	class WooHandlers {
		
		
		public static function change_add_to_cart_args( $args, $product ) {
			
			/** @var \WC_Product_Simple $product */
			
			if ( $product->is_purchasable() && $product->is_in_stock() && $product->get_type() === 'simple' ) {
				$args['attributes']['data-adscale_product_value'] = $product->get_price();
			}
			
			return $args;
		}
		
		
		
		
		public static function trigger_after_add_to_cart_event( $cart_item_key, $product_id, $quantity, $variation_id, $variation, $cart_item_data ) {
			
			/** @var \WC_Product_Simple $product */
			
			$product       = wc_get_product( $product_id );
			$product_price = $product->get_price();
			
			Assets::add_to_cart_trigger_js( $product_id, $product_price );
		}
		
		
		
		
		/**
		 * @param int $order_id
		 * @param array $posted_data
		 * @param \WC_Abstract_Order $order
		 */
		public static function after_checkout_order_processed( $order_id, $posted_data, $order ) {
			$adscaleLink = Helper::getAdscaleLinkId();
			
			if ( $order_id && $adscaleLink ) {
				update_post_meta( $order_id, '_adscale_link_id', $adscaleLink );
			}
			//Logger::log( "id_order: {$order_id}, adscaleLink: {$adscaleLink}", '', 'orderCreation' );
		}
		
		
		
		/**
		 * @param int $order_id
		 * @param \WC_Abstract_Order $order
		 */
		public static function order_status_changed( $order_id, $order ) {
			if ( ! is_admin() || wp_doing_ajax() ) {
				$cookie_name  = \str_replace( '%ORDER_ID%', $order_id, 'adscale_order_%ORDER_ID%' );
				$cookie_value = Helper::get_order_value( $order_id );
				
				if ( in_array( $order->get_status(), [ 'on-hold', 'processing', 'completed' ], true ) ) {
					Helper::setCookie( $cookie_name, $cookie_value );
					// for endpoint adscale/LastOrder
					Helper::setCookie( 'adscale__last_order_id', $order_id );
				}
				
				if ( in_array( $order->get_status(), [ 'failed', 'cancelled' ], true ) ) {
					Helper::setCookie( $cookie_name, '', 1 );
					// for endpoint adscale/LastOrder
					Helper::setCookie( 'adscale__last_order_id', '', 1 );
				}
			}
		}
		
		
		
		/**
		 * @param \WC_Abstract_Order $order
		 */
		public static function new_order( $order ) {
			if ( ! is_admin() || wp_doing_ajax() ) {
				$order_id     = $order->get_id();
				$cookie_name  = \str_replace( '%ORDER_ID%', $order_id, 'adscale_order_%ORDER_ID%' );
				$cookie_value = Helper::get_order_value( $order_id );
				
				if ( 'pending' === $order->get_status() ) {
					Helper::setCookie( $cookie_name, $cookie_value );
					//error_log( "setCookie fired[status:{$order->get_status()} name:{$cookie_name} value:{$cookie_value}].." );
				}
			}
		}
		
		
		
		public static function maybe_enable_wc_legacy_api() {
			$wc_legacy_api_auto_enable = Helper::getConfigSetting( 'shop/wc_legacy_api_auto_enable', false );
			
			if ( $wc_legacy_api_auto_enable ) {
				update_option( 'woocommerce_api_enabled', 'yes' );
			}
		}
		
		
	}
	
} // class_exists