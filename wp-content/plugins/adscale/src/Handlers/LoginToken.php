<?php

namespace AdScale\Handlers;

use AdScale\Helpers\Helper;


class LoginToken {
	
	public static function processLoginToken() {
		$token = Helper::generateRandHash();
		self::saveLoginToken( $token );
		
		return $token;
	}
	
	
	
	
	public static function getLoginToken() {
		return get_option( 'adscale__login_token', '' );
	}
	
	
	
	
	public static function saveLoginToken( $token ) {
		update_option( 'adscale__login_token', $token );
	}
	
	
	
	
	public static function clearLoginToken() {
		self::saveLoginToken( '' );
	}
}