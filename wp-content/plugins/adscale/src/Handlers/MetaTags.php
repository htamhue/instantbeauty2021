<?php

namespace AdScale\Handlers;


use AdScale\Helpers\Helper;



if ( ! class_exists( MetaTags::class ) ) {
	
	class MetaTags {
		
		
		public static function put_google_site_verification_tag() {
			$value = Helper::getGSVCode();
			$tag   = $value
				? "\n" . '<meta name="google-site-verification" content="' . esc_attr__( $value ) . '">' . "\n"
				: '';
			
			echo apply_filters( 'adscale/get_google_site_verification_tag', $tag );
		}
	}
	
} // class_exists