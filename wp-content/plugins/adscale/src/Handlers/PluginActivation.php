<?php

namespace AdScale\Handlers;

use AdScale\API\API_Manager;
use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;


if ( ! class_exists( PluginActivation::class ) ) {
	
	class PluginActivation {
		
		
		public static function activateHandler() {
			Helper::saveComputedShopDomain();
			Helper::saveDefaultConsiderPendingOrders();
			Helper::saveDefaultProxyEndpoint();
			
			Events::moduleEnableStarted();
			
			Logger::log( 'Plugin activation ...', '', 'plugin_activation' );
			API_Manager::process_api_gsv_code();
			WooHandlers::maybe_enable_wc_legacy_api();
			ShopKeys::process_shop_access_keys();
			update_option( 'adscale__need_activation_redirect', 1 );
			
			Events::moduleEnabled();
		}
		
		
		public static function deactivateHandler() {
			Logger::log( 'Plugin deactivation ...', '', 'plugin_activation' );
			Heartbeat::deactivate(); // deactivate heartbeat cron job
			delete_option( 'rewrite_rules' );
			Events::moduleDisable();
		}
		
		
		public static function redirectAfterActivation() {
			if ( get_option( 'adscale__need_activation_redirect', 0 ) ) {
				update_option( 'adscale__need_activation_redirect', 0 );
				if ( ! isset( $_GET['activate-multi'] ) ) {
					wp_redirect( GoToAdscale::getGoToAdscaleUrl() );
					exit;
				}
			}
		}
		
	}
	
} // class_exists