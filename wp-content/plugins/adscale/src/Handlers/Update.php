<?php

namespace AdScale\Handlers;

use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;
use AdScale\ServiceApi\ServiceApiExeption;


class Update {
	
	/**
	 * @param array $requestParams
	 *
	 * @throws ServiceApiExeption
	 */
	public static function actionProcessor( $requestParams = [] ) {
		$action   = Helper::getArrayValue( $requestParams, 'action' );
		$property = Helper::getArrayValue( $requestParams, 'property' );
		$value    = Helper::getArrayValue( $requestParams, 'value' );
		
		if ( 'modify' === $action && is_string( $property ) && $property ) {
			self::modifyProperty( $property, $value );
		}
		
		
		if ( 'create_api_credentials' === $action ) {
			self::createApiCredentials();
		}
		
		
		if ( 'enable_api' === $action ) {
			self::enableApi();
		}
		
		
		if ( 'disable_api' === $action ) {
			self::disableApi();
		}
	}
	
	
	
	
	public static function enableApi() {
		update_option( 'woocommerce_api_enabled', 'yes' );
		$savedValue  = get_option( 'woocommerce_api_enabled' );
		$saveSuccess = ( 'yes' === $savedValue );
		
		if ( $saveSuccess ) {
			Logger::log( 'action: enable_api', 'Success > ', 'process_Update' );
			// body == { success: true }
			Helper::sendResponseFormattedSuccess( [ 'success' => true ] );
		} else {
			Logger::log( 'action: enable_api', 'Fail > ', 'process_Update' );
		}
	}
	
	
	
	
	public static function disableApi() {
		update_option( 'woocommerce_api_enabled', 'no' );
		$savedValue  = get_option( 'woocommerce_api_enabled' );
		$saveSuccess = ( 'no' === $savedValue );
		
		if ( $saveSuccess ) {
			Logger::log( 'action: disable_api', 'Success > ', 'process_Update' );
			// body == { success: true }
			Helper::sendResponseFormattedSuccess( [ 'success' => true ] );
		} else {
			Logger::log( 'action: disable_api', 'Fail > ', 'process_Update' );
		}
	}
	
	
	
	
	public static function createApiCredentials() {
		// clear existing credentials
		// -------------------------- 
		
		// delete api user
		$user_login = sanitize_user( Helper::getConfigSetting( 'shop/keys_creation_user/login', 'adscale_app' ) );
		/** @var  $User \WP_User */
		$User    = get_user_by( 'login', $user_login );
		$user_id = 0;
		if ( $User instanceof \WP_User && ( $user_id = (int) $User->ID ) && ! is_multisite() ) {
			if ( ! function_exists( 'wp_delete_user' ) ) {
				require_once ABSPATH . 'wp-admin/includes/user.php';
			}
			wp_delete_user( $user_id );
		}
		// delete api key if exists
		if ( $user_id ) {
			ShopKeys::delete_shop_keys_data_by_user_id( $user_id );
		}
		// delete relative options
		delete_option( 'adscale__access_key_id' );
		delete_option( 'adscale__access_consumer_key' );
		delete_option( 'adscale__access_consumer_secret' );
		delete_option( 'adscale__access_wp_user_id' );
		
		
		// processing new credentials
		// -------------------------- 
		$keyData = ShopKeys::process_shop_access_keys();
		if (
			! empty( $keyData['key_id'] )
			&&
			(int) $keyData['key_id'] === (int) get_option( 'adscale__access_key_id' )
		) {
			Logger::log( 'action: create_api_credentials', 'Success > ', 'process_Update' );
			$keys_data = ShopKeys::get_keys_data();
			
			// body == json from the GetKeys endpoint
			Helper::sendResponseFormattedSuccess( [
				'shop_host'       => Helper::getShopDomain(),
				'platform'        => Helper::getConfigSetting( 'shop/platform', 'woo' ),
				'consumer_key'    => $keys_data['consumer_key'],
				'consumer_secret' => $keys_data['consumer_secret'],
				'version'         => ADSCALE_INTERNAL_MODULE_VERSION,
			] );
		} else {
			Logger::log( 'action: create_api_credentials', 'Fail > ', 'process_Update' );
		}
	}
	
	
	
	/**
	 * @param $propertyName
	 * @param $propertyValue
	 *
	 * @throws ServiceApiExeption
	 */
	public static function modifyProperty( $propertyName, $propertyValue ) {
		
		$dbOptionName = self::getDbOptionNameByPropertyName( $propertyName );
		
		if ( ! $dbOptionName ) {
			throw new ServiceApiExeption( 'System error: DbOptionName not found for PropertyName', '000' );
		}
		
		// process
		update_option( $dbOptionName, $propertyValue );
		$savedPropertyValue = get_option( $dbOptionName );
		$saveSuccess        = (
			$savedPropertyValue === $propertyValue ||
			maybe_serialize( $savedPropertyValue ) === maybe_serialize( $propertyValue ) ||
			$savedPropertyValue === (string) $propertyValue
		);
		
		if ( $saveSuccess ) {
			Logger::log( "action: modify, property: {$propertyName}, value: {$propertyValue}", 'Success > ',
				'process_Update' );
			
			//json format : {property: "%propertyName%", value: "%propertyValue%"}
			Helper::sendResponseFormattedSuccess( [ 'property' => $propertyName, 'value' => $propertyValue ] );
		}
		
		Logger::log( "action: modify, property: {$propertyName}, value: {$propertyValue}", 'Fail > '
			, 'process_Update' );
	}
	
	
	
	
	/**
	 * @param string $propertyName
	 *
	 * @return string|null DbOptionName
	 */
	public static function getDbOptionNameByPropertyName( $propertyName ) {
		$properties = [
			'shopHost'               => 'adscale__shop_domain',
			'googleVerificationCode' => 'adscale__gsv',
			'proxyEndpoint'          => 'adscale__proxy_endpoint',
			'considerPendingOrders'  => 'adscale__consider_pending_orders',
		];
		
		return isset( $properties[ $propertyName ] ) ? (string) $properties[ $propertyName ] : null;
	}
}
