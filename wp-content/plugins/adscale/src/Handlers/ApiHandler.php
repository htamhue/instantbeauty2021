<?php

namespace AdScale\Handlers;

use AdScale\ServiceApi\GetKeys as ServiceGetKeys;
use AdScale\ServiceApi\LoginToken as ServiceLoginToken;
use AdScale\ServiceApi\Update as ServiceUpdate;
use AdScale\ServiceApi\Status as ServiceStatus;
use AdScale\ServiceApi\Proxy as ServiceProxy;
use AdScale\ServiceApi\LastOrder as ServiceLastOrder;

class ApiHandler {
	
	private static $endpointsRules = [
		[
			'regex'      => '^adscale/GetKeys$',
			'query_vars' => [ 'adscaleGetKeys' => 1 ],
			'handler'    => [ ServiceGetKeys::class, 'initContent' ],
		],
		[
			'regex'      => '^adscale/LoginToken$',
			'query_vars' => [ 'adscaleLoginToken' => 1 ],
			'handler'    => [ ServiceLoginToken::class, 'initContent' ],
		],
		[
			'regex'      => '^adscale/Update$',
			'query_vars' => [ 'adscaleUpdate' => 1 ],
			'handler'    => [ ServiceUpdate::class, 'initContent' ],
		],
		[
			'regex'      => '^adscale/Status$',
			'query_vars' => [ 'adscaleStatus' => 1 ],
			'handler'    => [ ServiceStatus::class, 'initContent' ],
		],
		[
			'regex'      => '^adscale/Proxy$',
			'query_vars' => [ 'adscaleProxy' => 1 ],
			'handler'    => [ ServiceProxy::class, 'initContent' ],
		],
		[
			'regex'      => '^adscale/LastOrder$',
			'query_vars' => [ 'adscaleLastOrder' => 1 ],
			'handler'    => [ ServiceLastOrder::class, 'initContent' ],
		],
	];
	
	
	public static function handleApiRequests() {
		foreach ( self::$endpointsRules as $rule ) {
			if ( is_callable( $rule['handler'] ) ) {
				call_user_func( $rule['handler'] );
			}
		}
	}
	
	
	public static function addEndpoints() {
		$needRewriteOptionRefresh = false;
		$rewrite_rules            = get_option( 'rewrite_rules' );
		
		foreach ( self::$endpointsRules as $rule ) {
			$query = 'index.php?' . build_query( $rule['query_vars'] );
			
			add_rewrite_rule( $rule['regex'], $query, 'top' );
			
			if ( is_array( $rewrite_rules ) && ! array_key_exists( $rule['regex'], $rewrite_rules ) ) {
				$needRewriteOptionRefresh = true;
			}
			
			if ( $needRewriteOptionRefresh ) {
				delete_option( 'rewrite_rules' );
			}
		}
	}
	
	
	public static function addQueryVars( $vars ) {
		
		foreach ( self::$endpointsRules as $rule ) {
			if ( ! empty( $rule['query_vars'] ) ) {
				if ( is_array( $rule['query_vars'] ) ) {
					foreach ( $rule['query_vars'] as $var_name => $var_value ) {
						if ( is_string( $var_name ) ) {
							$vars[] = $var_name;
						}
					}
				} elseif ( is_string( $rule['query_vars'] ) ) {
					$vars[] = $rule['query_vars'];
				}
			}
		}
		
		return $vars;
	}
}
