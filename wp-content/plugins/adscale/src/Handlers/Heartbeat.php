<?php


namespace AdScale\Handlers;


use AdScale\App;
use AdScale\API\API_Manager;
use AdScale\Helpers\Helper;


class Heartbeat {
	
	
	public static function activate() {
		
		if ( wp_doing_ajax() ) {
			return;
		}
		
		$heartbeat_request_tpl = Helper::getConfigSetting( 'heartbeat/request_tpl', '' );
		$heartbeat_interval    = Helper::getConfigSetting( 'heartbeat/interval', '' );
		
		if ( ! $heartbeat_request_tpl || ! $heartbeat_interval ) {
			return;
		}
		
		add_filter( 'cron_schedules', function ( $schedules ) use ( $heartbeat_interval ) {
			$schedules['adscale_heartbeat'] = array(
				'interval' => $heartbeat_interval, // sec
				'display'  => 'Plugin AdScale / Heartbeat',
			);
			
			return $schedules;
		} );
		
		
		// CRON HOOK
		if ( ! wp_next_scheduled( 'adscale_heartbeat_hook' ) ) {
			wp_schedule_event( (int) current_time( 'timestamp' ), 'adscale_heartbeat', 'adscale_heartbeat_hook' );
		}
		
		// Action for HOOK
		add_action( 'adscale_heartbeat_hook', [ API_Manager::class, 'process_heartbeat' ] );
	}
	
	
	
	
	public static function deactivate() {
		if ( function_exists( '\wp_unschedule_hook' ) ) {
			\wp_unschedule_hook( 'adscale_heartbeat_hook' );
		} else {
			\wp_clear_scheduled_hook( 'adscale_heartbeat_hook' );
		}
	}
	
	
}