<?php

namespace AdScale\Handlers;

use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;


class GoToAdscale {
	
	const ACTION = 'adscale/admin/goToAdscale';
	const NONCE_ACTION = 'adscale_nonce_key';
	
	
	public static function getGoToAdscaleUrl() {
		$shopHost   = Helper::getShopDomain();
		$loginToken = LoginToken::processLoginToken();
		$email      = Helper::getResolvedActivationEmail();
		$platform   = Helper::getConfigSetting( 'shop/platform', 'woo' );
		
		return 'https://app.adscale.com/EcommerceStart' .
		       "?shopHost={$shopHost}&loginToken={$loginToken}&email={$email}&platform={$platform}";
	}
	
	
	public static function getEnableAdscaleBtnUrl() {
		$adminLink = admin_url( 'admin.php?page=wc_plugin_adscale_settings' );
		$token     = wp_create_nonce( self::NONCE_ACTION );
		
		return add_query_arg( [ 'action' => self::ACTION, 'nonce' => $token ], $adminLink );
	}
	
	
	public static function redirect() {
		if ( empty( $_GET['action'] ) || $_GET['action'] !== self::ACTION ) {
			return;
		}
		
		if ( empty( $_GET['nonce'] ) || ! wp_verify_nonce( $_GET['nonce'], self::NONCE_ACTION ) ) {
			Logger::log( 'Nonce error', 'getUrl : failed : ', 'GoToAdscale' );
			
			return;
		}
		
		$url = self::getGoToAdscaleUrl();
		
		if ( $url ) {
			Logger::log( $url, 'getUrl : success : ', 'GoToAdscale' );
			
			wp_redirect( $url );
			exit;
		} else {
			Logger::log( $url, 'getUrl : failed : ', 'GoToAdscale' );
			
			return;
		}
	}
}
