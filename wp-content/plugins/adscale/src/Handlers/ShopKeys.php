<?php

namespace AdScale\Handlers;

use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;


class ShopKeys {
	
	public static function process_shop_access_keys() {
		
		$app_name    = Helper::getConfigSetting( 'shop/keys_creation_params/app_name', 'AdScale App' );
		$app_user_id = Helper::getConfigSetting( 'shop/keys_creation_params/app_user_id', 'adscale_app' );
		$scope       = Helper::getConfigSetting( 'shop/keys_creation_params/scope', 'read' );
		
		$shop_user = self::get_shop_user();
		
		if ( ! $shop_user instanceof \WP_User ) {
			$shop_user_id = self::create_shop_user();
		} else {
			$shop_user_id = $shop_user->ID;
			// multisite
			if (
				is_multisite()
				&& ( $blog_id = get_current_blog_id() )
				&& ! is_user_member_of_blog( $shop_user_id, $blog_id )
			) {
				$role   = Helper::getConfigSetting( 'shop/keys_creation_user/role', 'shop_manager' );
				$result = add_user_to_blog( $blog_id, $shop_user_id, $role );
				if ( is_wp_error( $result ) ) {
					Logger::log( $result->get_error_message(), '[multisite]add_user_to_blog fail: ', 'process_shop_access_keys' );
				}
			}
		}
		
		
		if ( ! $exist_shop_keys_data = self::get_shop_keys_data_by_user_id( $shop_user_id ) ) {
			$created_shop_keys_data = self::create_shop_keys( $app_name, $app_user_id, $scope, $shop_user_id );
			//Logger::log( $created_shop_keys_data, 'create_shop_keys : $created_shop_keys_data : ', 'process_shop_access_keys' );
			Logger::log( ! empty( $created_shop_keys_data['key_id'] ), 'create_shop_keys: ', 'process_shop_access_keys' );
			self::save_keys_data( $created_shop_keys_data );
			
			return $created_shop_keys_data;
		}
		
		Logger::log( ! empty( $exist_shop_keys_data['key_id'] ), 'create_shop_keys : exist_shop_keys_data : ', 'process_shop_access_keys' );
		
		return $exist_shop_keys_data;
	}
	
	
	
	
	protected static function create_shop_keys( $app_name, $app_user_id, $scope, $shop_user_id ) {
		global $wpdb;
		
		if ( ! $shop_user_id ) {
			return false;
		}
		
		$description = sprintf(
		/* translators: 1: app name 2: scope 3: date 4: time */
			__( '%1$s - API %2$s (created on %3$s at %4$s).', 'adscale' ),
			wc_clean( $app_name ),
			$scope,
			date_i18n( wc_date_format() ),
			date_i18n( wc_time_format() )
		);
		
		
		// Created API keys.
		$permissions     = in_array( $scope, [ 'read', 'write', 'read_write' ], true ) ? sanitize_text_field( $scope ) : 'read';
		$consumer_key    = 'ck_' . wc_rand_hash();
		$consumer_secret = 'cs_' . wc_rand_hash();
		
		$wpdb->insert(
			$wpdb->prefix . 'woocommerce_api_keys',
			[
				'user_id'         => $shop_user_id,
				'description'     => $description,
				'permissions'     => $permissions,
				'consumer_key'    => wc_api_hash( $consumer_key ),
				'consumer_secret' => $consumer_secret,
				'truncated_key'   => substr( $consumer_key, - 7 ),
			],
			[
				'%d',
				'%s',
				'%s',
				'%s',
				'%s',
				'%s',
			]
		);
		
		return [
			'key_id'          => $wpdb->insert_id,
			'user_id'         => $app_user_id,
			'wp_user_id'      => $shop_user_id,
			'consumer_key'    => $consumer_key,
			'consumer_secret' => $consumer_secret,
			'key_permissions' => $permissions,
		];
	}
	
	
	
	
	public static function get_shop_keys_data_by_user_id( $shop_user_id ) {
		global $wpdb;
		
		$data = $wpdb->get_row(
			$wpdb->prepare(
				"
			SELECT key_id, user_id, permissions, consumer_key, consumer_secret, truncated_key
			FROM {$wpdb->prefix}woocommerce_api_keys
			WHERE user_id = %d
		",
				$shop_user_id
			),
			ARRAY_A
		);
		
		return $data;
	}
	
	
	
	
	public static function get_shop_keys_data_by_key_id( $key_id ) {
		global $wpdb;
		
		$data = $wpdb->get_row(
			$wpdb->prepare(
				"
			SELECT key_id, user_id, description, permissions, consumer_key, consumer_secret, truncated_key
			FROM {$wpdb->prefix}woocommerce_api_keys
			WHERE key_id = %d
		",
				$key_id
			),
			ARRAY_A
		);
		
		return $data;
	}
	
	
	
	
	public static function delete_shop_keys_data_by_user_id( $shop_user_id ) {
		global $wpdb;
		
		return $wpdb->delete( "{$wpdb->prefix}woocommerce_api_keys", [ 'user_id' => $shop_user_id ], [ '%d' ] );
	}
	
	
	
	
	protected static function create_shop_user() {
		
		$user_data               = [];
		$user_data['user_login'] = sanitize_user( Helper::getConfigSetting( 'shop/keys_creation_user/login', 'adscale_app' ) );
		$user_data['first_name'] = Helper::getConfigSetting( 'shop/keys_creation_user/name', 'AdScale App' );
		$user_data['user_email'] = Helper::getConfigSetting( 'shop/keys_creation_user/email', 'adscale.app.shop_user@gmail.com' );
		$user_data['user_pass']  = wp_generate_password( 16, true );
		$user_data['role']       = Helper::getConfigSetting( 'shop/keys_creation_user/role', 'shop_manager' );
		
		$enough_for_insert = (
			! empty( $user_data['user_login'] )
			&&
			! empty( $user_data['first_name'] )
			&&
			! empty( $user_data['user_email'] )
			&&
			! empty( $user_data['user_pass'] )
			&&
			! empty( $user_data['role'] )
		);
		
		if ( $enough_for_insert ) {
			$user_id = wp_insert_user( $user_data );
		} else {
			Logger::log( 'create_shop_user: not enough_for_insert', '', 'process_shop_access_keys' );
			
			return 0;
		}
		
		if ( is_wp_error( $user_id ) ) {
			Logger::log( 'create_shop_user: WP_Error : ' . $user_id->get_error_message(), '', 'process_shop_access_keys' );
			
			return 0;
		}
		
		
		update_user_meta( $user_id, 'is_adscale_api_user', 'yes' );
		
		return $user_id;
	}
	
	
	
	
	public static function get_shop_user() {
		$user_login = sanitize_user( Helper::getConfigSetting( 'shop/keys_creation_user/login', 'adscale_app' ) );
		
		return get_user_by( 'login', $user_login );
	}
	
	
	
	
	protected static function save_keys_data( $created_shop_keys_data ) {
		update_option( 'adscale__access_key_id', $created_shop_keys_data['key_id'] );
		update_option( 'adscale__access_consumer_key', $created_shop_keys_data['consumer_key'] );
		update_option( 'adscale__access_consumer_secret', $created_shop_keys_data['consumer_secret'] );
		update_option( 'adscale__access_wp_user_id', $created_shop_keys_data['wp_user_id'] );
	}
	
	
	
	
	public static function get_keys_data() {
		return [
			'key_id'          => get_option( 'adscale__access_key_id', '' ),
			'consumer_key'    => get_option( 'adscale__access_consumer_key', '' ),
			'consumer_secret' => get_option( 'adscale__access_consumer_secret', '' ),
			'wp_user_id'      => get_option( 'adscale__access_wp_user_id', '' ),
		];
	}
}