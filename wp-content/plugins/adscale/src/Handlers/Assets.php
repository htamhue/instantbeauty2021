<?php

namespace AdScale\Handlers;

use AdScale\App;
use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;


if ( ! class_exists( Assets::class ) ) {
	
	class Assets {
		
		
		public static function load_assets() {
			self::enqueue_adscale_script();
			self::add_product_vars();
			self::add_order_vars();
		}
		
		
		
		
		public static function load_admin_assets( $hook_suffix ) {
			if ( 'woocommerce_page_wc_plugin_adscale_settings' === $hook_suffix ) { // only plugin adscale page
				//self::enqueue_admin_script();
				//self::enqueue_admin_common_js_vars();
			}
		}
		
		
		
		
		public static function add_common_vars() {
			$js_vars = [];
			
			if ( $js_var_name = Helper::getConfigSetting( 'js_variables/cart_ajax_url', '' ) ) {
				$js_vars[ $js_var_name ] = "'" . esc_url( admin_url( 'admin-ajax.php' ) ) . "'";
			}
			if ( $js_var_name = Helper::getConfigSetting( 'js_variables/proxy_ajax_url', '' ) ) {
				$js_vars[ $js_var_name ] = "'" . esc_url( home_url( '/adscale/Proxy' ) ) . "'";
			}
			if ( $js_var_name = Helper::getConfigSetting( 'js_variables/last_order_ajax_url', '' ) ) {
				$js_vars[ $js_var_name ] = "'" . esc_url( home_url( '/adscale/LastOrder' ) ) . "'";
			}
			
			self::enqueue_js_vars( $js_vars );
		}
		
		
		
		
		public static function enqueue_adscale_script() {
			
			$adscale_script_url_base = Helper::getConfigSetting( 'adscale_script_url_base', '' );
			$adscale_script_handle   = Helper::getConfigSetting( 'adscale_script_handle', '' );
			
			if ( ! $adscale_script_url_base || ! $adscale_script_handle ) {
				return;
			}
			
			$shop_domain = Helper::getShopDomain();
			
			if ( ! $shop_domain ) {
				return;
			}
			
			$adscale_script_url = str_replace( '%SHOP_DOMAIN%', $shop_domain, $adscale_script_url_base );
			
			wp_enqueue_script( $adscale_script_handle, $adscale_script_url, [], time(), true );
			self::add_script_attr( $adscale_script_handle, 'async' );
			self::add_common_vars();
			self::add_to_cart_script();
		}
		
		
		
		// if add_to_cart via ajax
		public static function add_to_cart_script() {
			
			$script_handle = Helper::getConfigSetting( 'adscale_script_handle', '' );
			
			$js_var_name__product_id    = Helper::getConfigSetting( 'js_variables/product_id', '' );
			$js_var_name__product_value = Helper::getConfigSetting( 'js_variables/product_value', '' );
			
			if ( ! $script_handle ) {
				return;
			}
			
			$js_code = <<<EOT
(function ($) {
	$(window).on( 'load', function () {
		$(document.body).on('added_to_cart', function (e, fragments, cart_hash, this_button) {
			console.log('EVENT: added_to_cart');
			
			var productId = $(this_button).data('product_id') || window.$js_var_name__product_id,
				productValue = $(this_button).data('adscale_product_value') || window.$js_var_name__product_value;
			
			callAdscaleAddToCart(productId, productValue);
		});
		
		function callAdscaleAddToCart(productId, productValue) {
			console.log('productId: ', productId);
			console.log('productValue: ', productValue);
			
			if (!productId) {
				console.log('No productId !');
				return;
			}
			
			if (!productValue) {
				console.log('No productValue !');
				return;
			}
			
			try{
				if(typeof window.adscaleAddToCart === 'function'){
					adscaleAddToCart(productId, productValue);
				} else {
					console.log('func adscaleAddToCart not found!');
				}
			} catch(e){
				console.log('func adscaleAddToCart exception:' +  e.message );
			}
		}
	});
})(jQuery);
EOT;
			wp_add_inline_script( $script_handle, $js_code, 'before' );
			
		}
		
		
		
		
		public static function add_to_cart_trigger_js( $product_id, $product_value ) {
			
			$script_handle = Helper::getConfigSetting( 'adscale_script_handle', '' );
			
			if ( ! $script_handle ) {
				return;
			}
			
			$js_code = <<<EOT
(function ($) {
	$(window).on( 'load', function () {
		console.log('TRIGGER: added_to_cart');
		
		callAdscaleAddToCart($product_id, $product_value);
		
		function callAdscaleAddToCart(productId, productValue) {
			console.log('productId: ', productId);
			console.log('productValue: ', productValue);
			
			if (!productId) {
				console.log('No productId !');
				return;
			}
			
			if (!productValue) {
				console.log('No productValue !');
				return;
			}
			
			try{
				if(typeof window.adscaleAddToCart === 'function'){
					adscaleAddToCart(productId, productValue);
				} else {
					console.log('func adscaleAddToCart not found!');
				}
			} catch(e){
				console.log('func adscaleAddToCart exception:' +  e.message );
			}
		}
	});
})(jQuery);
EOT;
			add_action( 'wp_enqueue_scripts', function () use ( $script_handle, $js_code ) {
				wp_add_inline_script( $script_handle, $js_code, 'after' );
			}, 780 );
			
		}
		
		
		
		
		public static function add_product_vars() {
			
			if ( ! Helper::is_product_page() ) {
				return;
			}
			
			$js_var_name__product_id    = Helper::getConfigSetting( 'js_variables/product_id', '' );
			$js_var_name__product_value = Helper::getConfigSetting( 'js_variables/product_value', '' );
			
			if ( ! $js_var_name__product_id || ! $js_var_name__product_value ) {
				return;
			}
			
			self::enqueue_js_vars( [
				$js_var_name__product_id    => Helper::resolve_value_for_inline_js( Helper::get_product_id() ),
				$js_var_name__product_value => Helper::resolve_value_for_inline_js( Helper::get_product_value() ),
			] );
			
		}
		
		
		
		
		public static function add_order_vars() {
			$js_var_name__order_id       = Helper::getConfigSetting( 'js_variables/order_id', '' );
			$js_var_name__order_value    = Helper::getConfigSetting( 'js_variables/order_value', '' );
			$js_var_name__order_currency = Helper::getConfigSetting( 'js_variables/order_currency', '' );
			
			if ( ! $js_var_name__order_id || ! $js_var_name__order_value || ! $js_var_name__order_currency ) {
				return;
			}
			
			$is_order_received_endpoint = Helper::is_order_received_endpoint();
			$is_order_received_page     = Helper::is_order_received_page();
			
			if ( ! $is_order_received_endpoint && ! $is_order_received_page ) {
				return;
			}
			
			$order = null;
			
			if ( $is_order_received_endpoint ) {
				$order = Helper::get_order();
			} elseif ( $is_order_received_page ) {
				$order = Helper::get_order_by_get_params();
			}
			
			$order_id       = is_object( $order ) ? $order->get_id() : null;
			$order_value    = Helper::get_order_value( $order_id );
			$order_currency = is_object( $order ) ? $order->get_currency() : null;
			
			self::enqueue_js_vars( [
				$js_var_name__order_id       => Helper::resolve_value_for_inline_js( $order_id ),
				$js_var_name__order_value    => Helper::resolve_value_for_inline_js( $order_value ),
				$js_var_name__order_currency => Helper::resolve_value_for_inline_js( $order_currency ),
			] );
		}
		
		
		
		
		/**
		 * Add attributes to script tag such as 'async', 'defer'
		 * Script must be registered or enqueued before
		 *
		 * @param $handle_to_process
		 * @param $attr
		 */
		public static function add_script_attr( $handle_to_process, $attr ) {
			add_filter( 'script_loader_tag', function ( $tag, $handle, $src ) use ( $handle_to_process, $attr ) {
				$attr = esc_attr( $attr );
				
				if ( $handle_to_process === $handle ) {
					return str_replace( ' src', " {$attr} src", $tag );
				}
				
				return $tag;
			}, 10, 3 );
		}
		
		
		
		
		/**
		 * enqueue js vars before AdScale script
		 *
		 * @param array $js_vars
		 */
		public static function enqueue_js_vars( array $js_vars ) {
			$script_handle = Helper::getConfigSetting( 'adscale_script_handle', '' );
			if ( ! $script_handle ) {
				return;
			}
			
			foreach ( $js_vars as $var_name => $var_value ) {
				$js_code = "var {$var_name}={$var_value};";
				wp_add_inline_script( $script_handle, $js_code, 'before' );
			}
		}
		
		
		
		
		public static function enqueue_admin_script() {
			wp_enqueue_script(
				'adscale_admin',
				ADSCALE_PLUGIN_URL . 'assets/js/admin.js',
				[ 'jquery' ],
				ADSCALE_INTERNAL_MODULE_VERSION,
				true
			);
		}
		
		
		
		
		public static function enqueue_admin_common_js_vars() {
			$js_vars = [
				'ajaxUrl'   => esc_url( admin_url( 'admin-ajax.php' ) ),
				'ajaxNonce' => wp_create_nonce( 'adscale_nonce_key' ),
			];
			
			wp_localize_script( 'adscale_admin', 'adscaleJsVars', $js_vars );
		}
		
	}
	
} // class_exists