<?php

namespace AdScale\Handlers;

use AdScale\Helpers\Helper;


if ( ! class_exists( Settings::class ) ) {
	
	class Settings {
		
		
		public static $options_page_hook;
		
		
		
		
		public static function menu() {
			
			self::$options_page_hook = add_submenu_page(
				'woocommerce',
				__( 'AdScale', 'adscale' ),
				__( 'AdScale', 'adscale' ),
				'manage_woocommerce',
				'wc_plugin_adscale_settings',
				[ self::class, 'settings_page' ]
			);
			
		}
		
		
		
		
		public static function settings_page() {
			GoToAdscale::redirect();
			include ADSCALE_PLUGIN_DIR . '/src/views/settings-page.php';
		}
		
		
		
		
		public static function settings_init() {
			
			register_setting(
				'wc_plugin_adscale_settings__main_group',
				'adscale__shop_domain',
				[ self::class, 'sanitize__shop_domain' ]
			);
			
			register_setting(
				'wc_plugin_adscale_settings__main_group',
				'adscale__gsv',
				[ self::class, 'sanitize__gsv' ]
			);
			
			
			add_settings_section(
				'wc_plugin_adscale_settings__main_section',  // id секции , по которому нужно "цеплять" поля к секции
				'',
				[ self::class, 'main_section_desc_view' ],
				'wc_plugin_adscale_settings'
			);
			
			
			add_settings_field(
				'wc_plugin_adscale_settings__shop_domain',
				__( 'Shop domain (without protocol and www)', 'adscale' ),
				[ self::class, 'setting_view__shop_domain' ],
				'wc_plugin_adscale_settings',
				'wc_plugin_adscale_settings__main_section',
				[
					'tag_id'    => 'adscale__shop_domain_id',
					'tag_name'  => 'adscale__shop_domain',
					'label_for' => 'adscale__shop_domain_id',
				]
			);
			
			
			add_settings_field(
				'wc_plugin_adscale_settings__google_site_verification',
				__( 'Webmaster console verification (google-site-verification)', 'adscale' ),
				[ self::class, 'setting_view__google_site_verification' ],
				'wc_plugin_adscale_settings',
				'wc_plugin_adscale_settings__main_section',
				[
					'tag_id'    => 'adscale__gsv_id',
					'tag_name'  => 'adscale__gsv',
					'label_for' => 'adscale__gsv_id',
				]
			);
			
			
		}
		
		
		
		
		public static function main_section_desc_view() {
			//echo '<p>Main section</p>';
		}
		
		
		
		
		public static function setting_view__shop_domain( $args ) {
			
			$value = Helper::getShopDomain();
			
			echo '
				<input type="text" class="regular-text" readonly
					 id="' . esc_attr( $args['tag_id'] ) . '"
					 name="' . esc_attr( $args['tag_name'] ) . '"
					 value="' . esc_attr( $value ) . '">
				<br>
				<p class="description"></p>
			';
			
		}
		
		
		
		
		public static function setting_view__google_site_verification( $args ) {
			
			$value = Helper::getGSVCode();
			
			echo '
				<input type="text" class="regular-text" readonly
					 id="' . esc_attr( $args['tag_id'] ) . '"
					 name="' . esc_attr( $args['tag_name'] ) . '"
					 value="' . esc_attr( $value ) . '">
				 <br>
				 <p class="description"></p>
			';
		}
		
		
		
		
		public static function sanitize__shop_domain( $input ) {
			return sanitize_text_field( $input );
		}
		
		
		
		
		public static function sanitize__gsv( $input ) {
			return sanitize_text_field( $input );
		}
		
		
	}
	
} // class_exists