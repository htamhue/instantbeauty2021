<?php

namespace AdScale\Handlers;

use AdScale\Helpers\Helper;


class Status {
	
	
	public static function renderDataToJson( array $data ) {
		return json_encode( $data );
	}
	
	
	
	
	public static function renderDataToHtml( array $data ) {
		
		ob_start();
		?>
		<style>
			.adscale-debug {
				font-family: Arial, Helvetica, sans-serif;
			}
			
			.adscale-debug h3 {
				font-size: 20px;
				margin-top: 40px;
			}
			
			.adscale-debug-panel-desc {
				font-size: 16px;
			}
			
			.adscale-debug-table {
				background: #fff;
				border: 1px solid #ccd0d4;
				box-shadow: 0 1px 1px rgba(0, 0, 0, .04);
				border-spacing: 0;
				width: 100%;
				clear: both;
				margin: 0;
			}
			
			.adscale-debug-table * {
				word-wrap: break-word;
			}
			
			.adscale-debug-table > tbody > :nth-child(odd) {
				background-color: #f8f3fd;
			}
			
			.adscale-debug-table td {
				width: 35%;
				vertical-align: top;
				font-size: 14px;
				line-height: 1.5em;
			}
			
			.adscale-debug-table td:first-child {
				width: 25%;
			}
			
			.adscale-debug-table td:last-child {
				width: 40%;
			}
			
			.adscale-debug-table td, .adscale-debug-table th {
				padding: 8px 10px;
				color: #555;
			}
		</style>
		
		<div class="adscale-debug">
			<?php
			
			foreach ( $data as $section => $details ) {
				
				if ( ! isset( $details['fields'] ) || empty( $details['fields'] ) ) {
					continue;
				}
				?>
				
				<h3 class="adscale-debug-heading">
					<?php echo esc_html( $details['label'] ); ?>
					<?php
					if ( isset( $details['show_count'] ) && $details['show_count'] ) {
						printf( '(%d)', count( $details['fields'] ) );
					}
					?>
				</h3>
				
				<div class="adscale-debug-panel adscale-debug-panel-<?php echo esc_attr( $section ); ?>">
					<?php
					
					if ( isset( $details['description'] ) && ! empty( $details['description'] ) ) {
						printf( '<p class="adscale-debug-panel-desc">%s</p>', $details['description'] );
					}
					
					?>
					<table class="adscale-debug-table">
						<tbody>
						<?php
						
						foreach ( $details['fields'] as $field_name => $field ) {
							$label = ! empty( $field['label'] ) ? esc_html( $field['label'] ) : '';
							
							if ( is_array( $field['value'] ) ) {
								$values = '<ul>';
								
								foreach ( $field['value'] as $name => $value ) {
									$values .= sprintf( '<li>%s: %s</li>', esc_html( $name ), esc_html( $value ) );
								}
								
								$values .= '</ul>';
							} else {
								$values = ! empty( $field['value'] ) ? esc_html( $field['value'] ) : '';
							}
							
							$note = ! empty( $field['note'] ) ? esc_html( $field['note'] ) : '';
							
							printf( '<tr><td>%s</td><td>%s</td><td>%s</td></tr>', $label, $values, $note );
						}
						
						?>
						</tbody>
					</table>
				</div>
			
			<?php } ?>
		</div>
		
		<?php
		return ob_get_clean();
	}
	
	
	
	
	public static function getStatusData() {
		
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}
		if ( ! function_exists( 'get_theme_updates' ) ) {
			require_once ABSPATH . 'wp-admin/includes/update.php';
		}
		
		// Common vars
		$not_available = 'Not available';
		$is_multisite  = is_multisite();
		$upload_dir    = wp_upload_dir();
		
		// Set up the array that holds all debug information.
		$info = [];
		
		/*
		 * Data array structure
		 *
		 * (string) label       - The title for this section of the debug output.
		 * (string) description - Optional. A description for your information section.
		 * (boolean) show_count - Optional. If set to `true` the amount of fields will be included in the title for
		 *                                 this section.
		 * (array) fields {
		 *         An associative array containing the data to be displayed.
		 *
		 *    (string) label - The label for this piece of information.
		 *    (string) value - The output that is displayed for this field. Can be
		 *                       an associative array that is displayed as name/value pairs.
		 *    (mixed) debug  - Optional. The raw data value for this field(genuine value).
		 *                       If not set, the content of `value` is used.
		 *    (string) note -  Optional. Some additional information. Warnings, notes, errors etc.
		 * }
		 */
		
		/*
		|--------------------------------------------------------------------------
		| Sections init
		|--------------------------------------------------------------------------
		*/
		
		$info['general'] = [
			'label'  => 'General',
			'fields' => [],
		];
		
		$info['server'] = [
			'label'       => 'Server',
			'description' => 'The options shown below relate to server setup.',
			'fields'      => [],
		];
		
		$info['adscale-plugin'] = [
			'label'  => 'Adscale plugin',
			'fields' => [],
		];
		
		$info['wp-shop-api'] = [
			'label'  => 'Shop API',
			'fields' => [],
		];
		
		$info['wp-permalinks'] = [
			'label'  => 'Permalinks',
			'fields' => [],
		];
		
		$info['wp-dropins'] = [
			'label'       => 'Drop-ins',
			'show_count'  => true,
			'description' => sprintf(
				'Drop-ins are single files, found in the %s directory, that replace or enhance WordPress features in ways that are not possible for traditional plugins.',
				'<code>' . str_replace( ABSPATH, '', WP_CONTENT_DIR ) . '</code>'
			),
			'fields'      => [],
		];
		
		$info['wp-active-theme'] = [
			'label'  => 'Active Theme',
			'fields' => [],
		];
		
		$info['wp-parent-theme'] = [
			'label'  => 'Parent Theme',
			'fields' => [],
		];
		
		$info['wp-mu-plugins'] = [
			'label'      => 'Must Use Plugins',
			'show_count' => true,
			'fields'     => [],
		];
		
		$info['wp-plugins-active'] = [
			'label'      => 'Active Plugins',
			'show_count' => true,
			'fields'     => [],
		];
		
		$info['wp-plugins-inactive'] = [
			'label'      => 'Inactive Plugins',
			'show_count' => true,
			'fields'     => [],
		];
		
		$info['wp-constants'] = [
			'label'       => 'WordPress Constants',
			'description' => 'These settings alter where and how parts of WordPress are loaded.',
			'fields'      => [],
		];
		
		$info['wp-paths'] = [
			'label'  => 'Directories',
			'fields' => [],
		];
		
		$info['wp-filesystem'] = [
			'label'       => 'Filesystem Permissions',
			'description' => 'Shows whether WordPress is able to write to the directories it needs access to.',
			'fields'      => [],
		];
		
		
		
		/*
		|--------------------------------------------------------------------------
		| Section: general
		|--------------------------------------------------------------------------
		*/
		
		$core_version       = get_bloginfo( 'version' );
		$cms_email          = get_option( 'admin_email' );
		$cms_urls_rewriting = (bool) get_option( 'permalink_structure' );
		
		$info['general']['fields'] = [
			'platform'           => [
				'label' => 'Platform',
				'value' => 'Wordpress',
				'debug' => 'wordpress',
			],
			'platform_version'   => [
				'label' => 'Platform version',
				'value' => $core_version,
				'debug' => $core_version,
			],
			'site_url'           => [
				'label' => 'Site URL',
				'value' => get_bloginfo( 'url' ),
				'debug' => get_bloginfo( 'url' ),
			],
			'site_path'          => [
				'label' => 'Site path',
				'value' => untrailingslashit( ABSPATH ),
				'debug' => untrailingslashit( ABSPATH ),
			],
			'cms_email'          => [
				'label' => 'Email',
				'value' => $cms_email,
			],
			'cms_urls_rewriting' => [
				'label' => 'Urls rewriting (Friendly URL)',
				'value' => $cms_urls_rewriting ? 'Enabled' : 'Disabled',
				'debug' => $cms_urls_rewriting,
				'note'  => ( $cms_urls_rewriting
					? ''
					: 'Attention!'
				),
			],
			'is_multisite'       => [
				'label' => 'is_multisite',
				'value' => $is_multisite ? 'Enabled' : 'Disabled',
				'debug' => $is_multisite,
			],
		];
		
		/*
		|--------------------------------------------------------------------------
		| Section: server
		|--------------------------------------------------------------------------
		*/
		
		// Populate the server debug fields.
		if ( function_exists( 'php_uname' ) ) {
			$server_architecture = sprintf( '%s %s %s', php_uname( 's' ), php_uname( 'r' ), php_uname( 'm' ) );
		} else {
			$server_architecture = 'unknown';
		}
		
		if ( function_exists( 'phpversion' ) ) {
			$php_version_debug = phpversion();
			// Whether PHP supports 64bit
			$php64bit = ( PHP_INT_SIZE * 8 === 64 );
			
			$php_version = sprintf(
				'%s %s',
				$php_version_debug,
				( $php64bit ? '(Supports 64bit values)' : '(Does not support 64bit values)' )
			);
			
			if ( $php64bit ) {
				$php_version_debug .= ' 64bit';
			}
		} else {
			$php_version       = 'Unable to determine PHP version';
			$php_version_debug = 'unknown';
		}
		
		if ( function_exists( 'php_sapi_name' ) ) {
			$php_sapi                      = php_sapi_name();
			$php_sapi_is_cgi_subtype_debug = stripos( $php_sapi, 'cgi' ) !== false;
			$php_sapi_is_cgi_subtype       = $php_sapi_is_cgi_subtype_debug ? 'Yes' : 'No';
		} else {
			$php_sapi                      = 'unknown';
			$php_sapi_is_cgi_subtype_debug = 'unknown';
			$php_sapi_is_cgi_subtype       = 'unknown';
		}
		
		if ( function_exists( '\json_decode' ) && function_exists( '\json_last_error' ) ) {
			$json_extension_debug = true;
			$json_extension_note  = '';
		} else {
			$json_extension_debug = false;
			$json_extension_note  = 'Attention! Adscale plugin does not work without JSON extension!';
		}
		
		$info['server']['fields']['server_architecture']     = [
			'label' => 'Server architecture',
			'value' => ( 'unknown' !== $server_architecture ? $server_architecture : 'Unable to determine server architecture' ),
			'debug' => $server_architecture,
		];
		$info['server']['fields']['httpd_software']          = [
			'label' => 'Web server',
			'value' => ( isset( $_SERVER['SERVER_SOFTWARE'] ) ? $_SERVER['SERVER_SOFTWARE'] : 'Unable to determine what web server software is used' ),
			'debug' => ( isset( $_SERVER['SERVER_SOFTWARE'] ) ? $_SERVER['SERVER_SOFTWARE'] : 'unknown' ),
		];
		$info['server']['fields']['php_version']             = [
			'label' => 'PHP version',
			'value' => $php_version,
			'debug' => $php_version_debug,
		];
		$info['server']['fields']['php_sapi']                = [
			'label' => 'PHP Server API (SAPI)',
			'value' => ( 'unknown' !== $php_sapi ? $php_sapi : 'Unable to determine PHP SAPI' ),
			'debug' => $php_sapi,
		];
		$info['server']['fields']['php_sapi_is_cgi_subtype'] = [
			'label' => 'PHP Server API is subtype of CGI',
			'value' => ( 'unknown' !== $php_sapi_is_cgi_subtype ? $php_sapi_is_cgi_subtype : 'Unable to determine PHP SAPI' ),
			'debug' => $php_sapi_is_cgi_subtype_debug,
		];
		
		// Some servers disable `ini_set()` and `ini_get()`, we check this before trying to get configuration values.
		if ( ! function_exists( 'ini_get' ) ) {
			$info['server']['fields']['ini_get'] = [
				'label' => 'Server settings',
				'value' => sprintf(
					'Unable to determine some settings, as the %s function has been disabled.',
					'ini_get()'
				),
				'debug' => 'ini_get() is disabled',
			];
		} else {
			$info['server']['fields']['max_input_variables'] = [
				'label' => 'PHP max input variables',
				'value' => ini_get( 'max_input_vars' ),
			];
			$info['server']['fields']['time_limit']          = [
				'label' => 'PHP time limit',
				'value' => ini_get( 'max_execution_time' ),
			];
			$info['server']['fields']['memory_limit']        = [
				'label' => 'PHP memory limit',
				'value' => ini_get( 'memory_limit' ),
			];
			$info['server']['fields']['max_input_time']      = [
				'label' => 'Max input time',
				'value' => ini_get( 'max_input_time' ),
			];
			$info['server']['fields']['upload_max_size']     = [
				'label' => 'Upload max filesize',
				'value' => ini_get( 'upload_max_filesize' ),
			];
			$info['server']['fields']['php_post_max_size']   = [
				'label' => 'PHP post max size',
				'value' => ini_get( 'post_max_size' ),
			];
		}
		
		if ( function_exists( 'curl_version' ) ) {
			$curl = curl_version();
			
			$info['server']['fields']['curl_version'] = [
				'label' => 'cURL version',
				'value' => sprintf( '%s %s', $curl['version'], $curl['ssl_version'] ),
			];
		} else {
			$info['server']['fields']['curl_version'] = [
				'label' => 'cURL version',
				'value' => $not_available,
				'debug' => 'not available',
			];
		}
		
		
		$info['server']['fields']['json_extension'] = [
			'label' => 'JSON extension',
			'value' => $json_extension_debug ? 'Enabled' : 'Disabled',
			'debug' => $json_extension_debug,
		];
		
		if ( $json_extension_note ) {
			$info['server']['fields']['json_extension']['note'] = $json_extension_note;
		}
		
		
		if ( function_exists( 'apache_get_modules' ) ) {
			$apache_modules       = apache_get_modules();
			$mod_auth_basic_debug = in_array( 'mod_auth_basic', $apache_modules, true );
			$mod_rewrite_debug    = in_array( 'mod_rewrite', $apache_modules, true );
			
			$info['server']['fields']['apache_get_modules'] = [
				'label' => 'Apache module check available',
				'value' => 'Yes',
				'debug' => true,
			];
			
			$info['server']['fields']['mod_auth_basic'] = [
				'label' => 'Apache module check: mod_auth_basic',
				'value' => $mod_auth_basic_debug ? 'Enabled' : 'Disabled',
				'debug' => $mod_auth_basic_debug,
				'note'  => ( $mod_auth_basic_debug
					? ''
					: 'Attention! See https://woocommerce.github.io/woocommerce-rest-api-docs/#authentication-over-https'
				),
			];
			
			$info['server']['fields']['mod_rewrite'] = [
				'label' => 'Apache module check: mod_rewrite',
				'value' => $mod_rewrite_debug ? 'Enabled' : 'Disabled',
				'debug' => $mod_rewrite_debug,
				'note'  => ( $mod_rewrite_debug
					? ''
					: 'Attention! Please activate the "mod_rewrite" Apache module
                     to allow the Shop API.'
				),
			];
		} else {
			$info['server']['fields']['apache_get_modules'] = [
				'label' => 'Apache module check available',
				'value' => 'No',
				'debug' => false,
				'note'  => 'Attention! Impossible to check to see if basic authentication and rewrite extensions 
                  have been activated. Please manually check if they\'ve been activated in order to use the
                  Shop API. See https://woocommerce.github.io/woocommerce-rest-api-docs/#authentication-over-https',
			];
		}
		
		/*
		|--------------------------------------------------------------------------
		| Section: adscale-plugin
		|--------------------------------------------------------------------------
		*/
		
		$shopHost               = Helper::getShopDomain();
		$googleVerificationCode = Helper::getGSVCode();
		$proxyEndpoint          = Helper::getProxyEndpoint();
		$considerPendingOrders  = Helper::isConsiderPendingOrders();
		
		$info['adscale-plugin']['fields'] = [
			'internal_version'       => [
				'label' => 'Internal version',
				'value' => ( defined( 'ADSCALE_INTERNAL_MODULE_VERSION' )
					? ADSCALE_INTERNAL_MODULE_VERSION : 'Undefined' ),
				'debug' => ( defined( 'ADSCALE_INTERNAL_MODULE_VERSION' )
					? ADSCALE_INTERNAL_MODULE_VERSION : 'undefined' ),
			],
			'shopHost'               => [
				'label' => 'shopHost',
				'value' => $shopHost,
				'note'  => $shopHost ? '' : 'Attention! Empty property shopHost',
			],
			'googleVerificationCode' => [
				'label' => 'googleVerificationCode',
				'value' => $googleVerificationCode,
				'note'  => $googleVerificationCode ? '' : 'Attention! Empty property googleVerificationCode',
			],
			'proxyEndpoint'          => [
				'label' => 'proxyEndpoint',
				'value' => $proxyEndpoint,
				'note'  => $proxyEndpoint ? '' : 'Attention! Empty property proxyEndpoint',
			],
			'considerPendingOrders'  => [
				'label' => 'considerPendingOrders',
				'value' => $considerPendingOrders ? 'Yes' : 'No',
				'debug' => $considerPendingOrders,
			],
		];
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-shop-api
		|--------------------------------------------------------------------------
		*/
		
		// webservice
		$ws_debug = 'yes' === get_option( 'woocommerce_api_enabled' );
		$ws       = $ws_debug ? 'Enabled' : 'Disabled';
		
		// webservice_adscale_key
		
		// get data from adscale plugin options
		$ws_adscale_key_data = ShopKeys::get_keys_data();
		//echo '<pre>'; echo var_export($ws_adscale_key_data, true); echo '</pre>';  die();
		$ws_adscale_key_id              = (int) $ws_adscale_key_data['key_id'];
		$ws_adscale_key_consumer_key    = $ws_adscale_key_data['consumer_key'];
		$ws_adscale_key_consumer_secret = $ws_adscale_key_data['consumer_secret'];
		
		// get data from db
		$ws_adscale_key_data_db = ShopKeys::get_shop_keys_data_by_key_id( $ws_adscale_key_id );
		$ws_adscale_key_user    = ! empty( $ws_adscale_key_data_db['user_id'] )
			? get_user_by( 'id', $ws_adscale_key_data_db['user_id'] )
			: null;
		
		$ws_adscale_key_exists  = ! empty( $ws_adscale_key_data_db );
		$ws_adscale_user_exists = $ws_adscale_key_user instanceof \WP_User;
		
		if ( $is_multisite ) {
			$ws_adscale_user_exists = $ws_adscale_user_exists
			                          && is_user_member_of_blog( $ws_adscale_key_user->ID, get_current_blog_id() );
		}
		
		
		$info['wp-shop-api']['fields'] = [
			'webservice'                    => [
				'label' => 'Webservice',
				'value' => $ws,
				'debug' => $ws_debug,
				'note'  => $ws_debug ? '' : 'Attention! The Webservice must be enable for access',
			],
			'webservice_adscale_key'        => [
				'label' => 'Webservice API Key for Adscale (consumer_key)',
				'value' => $ws_adscale_key_exists ? $ws_adscale_key_consumer_key : '',
				'note'  => $ws_adscale_key_exists
					? $ws_adscale_key_data_db['description']
					: 'Attention! There is no key for access',
			],
			'webservice_adscale_key_secret' => [
				'label' => 'Webservice API Key for Adscale (consumer_secret)',
				'value' => $ws_adscale_key_exists ? $ws_adscale_key_consumer_secret : '',
			],
		];
		
		if ( $ws_adscale_key_exists ) {
			$info['wp-shop-api']['fields']['webservice_adscale_key_user'] = [
				'label' => 'User for Webservice API Key for Adscale',
				'value' => $ws_adscale_user_exists ? "{$ws_adscale_key_user->user_login} ({$ws_adscale_key_user->user_email})" : 'Missing',
				'debug' => $ws_adscale_user_exists ? "login:{$ws_adscale_key_user->user_login}, email:{$ws_adscale_key_user->user_email}" : '',
				'note'  => $ws_adscale_user_exists ? '' : 'Attention! There is no user for key access',
			];
		}
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-permalinks
		|--------------------------------------------------------------------------
		*/
		
		$permalink_structure = get_option( 'permalink_structure' );
		$category_base       = get_option( 'category_base' );
		$tag_base            = get_option( 'tag_base' );
		
		$wc_permalinks = Helper::getWcPermalinkStructure();
		
		$wc_category_base  = isset( $wc_permalinks['category_base'] ) ? $wc_permalinks['category_base'] : '';
		$wc_tag_base       = isset( $wc_permalinks['tag_base'] ) ? $wc_permalinks['tag_base'] : '';
		$wc_attribute_base = isset( $wc_permalinks['attribute_base'] ) ? $wc_permalinks['attribute_base'] : '';
		$wc_product_base   = isset( $wc_permalinks['product_base'] ) ? $wc_permalinks['product_base'] : '';
		
		$info['wp-permalinks']['fields'] = [
			'permalink_structure' => [
				'label' => 'Permalink structure',
				'value' => $permalink_structure,
			],
			'category_base'       => [
				'label' => 'Category base',
				'value' => $category_base,
			],
			'tag_base'            => [
				'label' => 'Tag base',
				'value' => $tag_base,
			],
			// woocommerce
			'wc_category_base'    => [
				'label' => 'Product category base',
				'value' => $wc_category_base,
			],
			'wc_tag_base'         => [
				'label' => 'Product tag base',
				'value' => $wc_tag_base,
			],
			'wc_attribute_base'   => [
				'label' => 'Product attribute base',
				'value' => $wc_attribute_base,
			],
			'wc_product_base'     => [
				'label' => 'Product permalinks',
				'value' => $wc_product_base,
			],
		];
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-dropins
		|--------------------------------------------------------------------------
		*/
		
		// Get a list of all drop-in replacements.
		$dropins = get_dropins();
		
		// Get dropins descriptions.
		$dropin_descriptions = _get_dropins();
		
		foreach ( $dropins as $dropin_key => $dropin ) {
			$info['wp-dropins']['fields'][ sanitize_text_field( $dropin_key ) ] = [
				'label' => $dropin_key,
				'value' => $dropin_descriptions[ $dropin_key ][0],
				'debug' => 'true',
			];
		}
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-active-theme
		|--------------------------------------------------------------------------
		*/
		
		// Populate the section for the currently active theme.
		$active_theme  = wp_get_theme();
		$theme_updates = get_theme_updates();
		
		$active_theme_version       = $active_theme->Version;
		$active_theme_version_debug = $active_theme_version;
		
		if ( array_key_exists( $active_theme->stylesheet, $theme_updates ) ) {
			$theme_update_new_version   = $theme_updates[ $active_theme->stylesheet ]->update['new_version'];
			$active_theme_version       .= ' ' . sprintf( __( '(Latest version: %s)' ), $theme_update_new_version );
			$active_theme_version_debug .= sprintf( ' (latest version: %s)', $theme_update_new_version );
		}
		
		$active_theme_author_uri = $active_theme->offsetGet( 'Author URI' );
		
		if ( $active_theme->parent_theme ) {
			$active_theme_parent_theme       = sprintf(
				'%1$s (%2$s)',
				$active_theme->parent_theme,
				$active_theme->template
			);
			$active_theme_parent_theme_debug = sprintf(
				'%s (%s)',
				$active_theme->parent_theme,
				$active_theme->template
			);
		} else {
			$active_theme_parent_theme       = 'None';
			$active_theme_parent_theme_debug = 'none';
		}
		
		$info['wp-active-theme']['fields'] = [
			'name'           => [
				'label' => 'Name',
				'value' => sprintf(
					'%1$s (%2$s)',
					$active_theme->Name,
					$active_theme->stylesheet
				),
			],
			'version'        => [
				'label' => 'Version',
				'value' => $active_theme_version,
				'debug' => $active_theme_version_debug,
			],
			'parent_theme'   => [
				'label' => 'Parent theme',
				'value' => $active_theme_parent_theme,
				'debug' => $active_theme_parent_theme_debug,
			],
			'theme_path'     => [
				'label' => 'Theme directory location',
				'value' => get_stylesheet_directory(),
			],
			'author'         => [
				'label' => 'Author',
				'value' => wp_kses( $active_theme->Author, [] ),
			],
			'author_website' => [
				'label' => 'Author website',
				'value' => ( $active_theme_author_uri ? $active_theme_author_uri : 'Undefined' ),
				'debug' => ( $active_theme_author_uri ? $active_theme_author_uri : '(undefined)' ),
			],
		];
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-parent-theme
		|--------------------------------------------------------------------------
		*/
		
		$parent_theme = $active_theme->parent();
		
		if ( $parent_theme ) {
			$parent_theme_version       = $parent_theme->Version;
			$parent_theme_version_debug = $parent_theme_version;
			
			if ( array_key_exists( $parent_theme->stylesheet, $theme_updates ) ) {
				$parent_theme_update_new_version = $theme_updates[ $parent_theme->stylesheet ]->update['new_version'];
				$parent_theme_version            .= ' ' . sprintf( '(Latest version: %s)', $parent_theme_update_new_version );
				$parent_theme_version_debug      .= sprintf( ' (latest version: %s)', $parent_theme_update_new_version );
			}
			
			$parent_theme_author_uri = $parent_theme->offsetGet( 'Author URI' );
			
			$info['wp-parent-theme']['fields'] = [
				'name'           => [
					'label' => 'Name',
					'value' => sprintf(
						'%1$s (%2$s)',
						$parent_theme->Name,
						$parent_theme->stylesheet
					),
				],
				'version'        => [
					'label' => 'Version',
					'value' => $parent_theme_version,
					'debug' => $parent_theme_version_debug,
				],
				'author'         => [
					'label' => 'Author',
					'value' => wp_kses( $parent_theme->Author, [] ),
				],
				'author_website' => [
					'label' => 'Author website',
					'value' => ( $parent_theme_author_uri ? $parent_theme_author_uri : 'Undefined' ),
					'debug' => ( $parent_theme_author_uri ? $parent_theme_author_uri : '(undefined)' ),
				],
				'theme_path'     => [
					'label' => 'Theme directory location',
					'value' => get_template_directory(),
				],
			];
		}
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-mu-plugins
		|--------------------------------------------------------------------------
		*/
		
		// List must use plugins if there are any.
		$mu_plugins = get_mu_plugins();
		
		foreach ( $mu_plugins as $plugin_path => $plugin ) {
			$plugin_version = $plugin['Version'];
			$plugin_author  = $plugin['Author'];
			
			$plugin_version_string       = 'No version or author information is available.';
			$plugin_version_string_debug = 'author: (undefined), version: (undefined)';
			
			if ( ! empty( $plugin_version ) && ! empty( $plugin_author ) ) {
				$plugin_version_string       = sprintf( 'Version %1$s by %2$s', $plugin_version, $plugin_author );
				$plugin_version_string_debug = sprintf( 'version: %s, author: %s', $plugin_version, $plugin_author );
			} else {
				if ( ! empty( $plugin_author ) ) {
					$plugin_version_string       = sprintf( 'By %s', $plugin_author );
					$plugin_version_string_debug = sprintf( 'author: %s, version: (undefined)', $plugin_author );
				}
				
				if ( ! empty( $plugin_version ) ) {
					$plugin_version_string       = sprintf( 'Version %s', $plugin_version );
					$plugin_version_string_debug = sprintf( 'author: (undefined), version: %s', $plugin_version );
				}
			}
			
			$info['wp-mu-plugins']['fields'][ sanitize_text_field( $plugin['Name'] ) ] = [
				'label' => $plugin['Name'],
				'value' => $plugin_version_string,
				'debug' => $plugin_version_string_debug,
			];
		}
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-plugins-active
		| Section: wp-plugins-inactive
		|--------------------------------------------------------------------------
		*/
		
		// List all available plugins.
		$plugins        = get_plugins();
		$plugin_updates = function_exists( 'get_plugin_updates' ) ? get_plugin_updates() : [];
		
		
		foreach ( $plugins as $plugin_path => $plugin ) {
			$plugin_part = ( is_plugin_active( $plugin_path ) ) ? 'wp-plugins-active' : 'wp-plugins-inactive';
			/*
			if ( ! is_plugin_active( $plugin_path ) ) {
				continue;
			}
			$plugin_part = 'wp-plugins-active';
			*/
			$plugin_version = $plugin['Version'];
			$plugin_author  = $plugin['Author'];
			
			$plugin_version_string       = 'No version or author information is available.';
			$plugin_version_string_debug = 'author: (undefined), version: (undefined)';
			
			if ( ! empty( $plugin_version ) && ! empty( $plugin_author ) ) {
				$plugin_version_string       = sprintf( 'Version %1$s by %2$s', $plugin_version, $plugin_author );
				$plugin_version_string_debug = sprintf( 'version: %s, author: %s', $plugin_version, $plugin_author );
			} else {
				if ( ! empty( $plugin_author ) ) {
					$plugin_version_string       = sprintf( 'By %s', $plugin_author );
					$plugin_version_string_debug = sprintf( 'author: %s, version: (undefined)', $plugin_author );
				}
				
				if ( ! empty( $plugin_version ) ) {
					$plugin_version_string       = sprintf( 'Version %s', $plugin_version );
					$plugin_version_string_debug = sprintf( 'author: (undefined), version: %s', $plugin_version );
				}
			}
			
			if ( array_key_exists( $plugin_path, $plugin_updates ) ) {
				$plugin_version_string       .= ' ' . sprintf( '(Latest version: %s)', $plugin_updates[ $plugin_path ]->update->new_version );
				$plugin_version_string_debug .= sprintf( ' (latest version: %s)', $plugin_updates[ $plugin_path ]->update->new_version );
			}
			
			$info[ $plugin_part ]['fields'][ sanitize_text_field( $plugin['Name'] ) ] = [
				'label' => $plugin['Name'],
				'value' => $plugin_version_string,
				'debug' => $plugin_version_string_debug,
			];
		}
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-constants
		|--------------------------------------------------------------------------
		*/
		
		// Check if WP_DEBUG_LOG is set.
		$wp_debug_log_value = 'Disabled';
		
		if ( is_string( WP_DEBUG_LOG ) ) {
			$wp_debug_log_value = WP_DEBUG_LOG;
		} elseif ( WP_DEBUG_LOG ) {
			$wp_debug_log_value = 'Enabled';
		}
		
		// Check CONCATENATE_SCRIPTS.
		if ( defined( 'CONCATENATE_SCRIPTS' ) ) {
			$concatenate_scripts       = CONCATENATE_SCRIPTS ? 'Enabled' : 'Disabled';
			$concatenate_scripts_debug = CONCATENATE_SCRIPTS ? 'true' : 'false';
		} else {
			$concatenate_scripts       = 'Undefined';
			$concatenate_scripts_debug = 'undefined';
		}
		
		// Check COMPRESS_SCRIPTS.
		if ( defined( 'COMPRESS_SCRIPTS' ) ) {
			$compress_scripts       = COMPRESS_SCRIPTS ? 'Enabled' : 'Disabled';
			$compress_scripts_debug = COMPRESS_SCRIPTS ? 'true' : 'false';
		} else {
			$compress_scripts       = 'Undefined';
			$compress_scripts_debug = 'undefined';
		}
		
		// Check COMPRESS_CSS.
		if ( defined( 'COMPRESS_CSS' ) ) {
			$compress_css       = COMPRESS_CSS ? 'Enabled' : 'Disabled';
			$compress_css_debug = COMPRESS_CSS ? 'true' : 'false';
		} else {
			$compress_css       = 'Undefined';
			$compress_css_debug = 'undefined';
		}
		
		// Check WP_LOCAL_DEV.
		if ( defined( 'WP_LOCAL_DEV' ) ) {
			$wp_local_dev       = WP_LOCAL_DEV ? 'Enabled' : 'Disabled';
			$wp_local_dev_debug = WP_LOCAL_DEV ? 'true' : 'false';
		} else {
			$wp_local_dev       = 'Undefined';
			$wp_local_dev_debug = 'undefined';
		}
		
		// Check DISABLE_WP_CRON.
		if ( defined( 'DISABLE_WP_CRON' ) ) {
			$disable_wp_cron       = DISABLE_WP_CRON ? 'Enabled' : 'Disabled';
			$disable_wp_cron_debug = DISABLE_WP_CRON ? 'true' : 'false';
		} else {
			$disable_wp_cron       = 'Undefined';
			$disable_wp_cron_debug = 'undefined';
		}
		
		$info['wp-constants']['fields'] = [
			'ABSPATH'             => [
				'label' => 'ABSPATH',
				'value' => ABSPATH,
			],
			'WP_HOME'             => [
				'label' => 'WP_HOME',
				'value' => ( defined( 'WP_HOME' ) ? WP_HOME : 'Undefined' ),
				'debug' => ( defined( 'WP_HOME' ) ? WP_HOME : 'undefined' ),
			],
			'WP_SITEURL'          => [
				'label' => 'WP_SITEURL',
				'value' => ( defined( 'WP_SITEURL' ) ? WP_SITEURL : 'Undefined' ),
				'debug' => ( defined( 'WP_SITEURL' ) ? WP_SITEURL : 'undefined' ),
			],
			'WP_CONTENT_DIR'      => [
				'label' => 'WP_CONTENT_DIR',
				'value' => WP_CONTENT_DIR,
			],
			'WP_PLUGIN_DIR'       => [
				'label' => 'WP_PLUGIN_DIR',
				'value' => WP_PLUGIN_DIR,
			],
			'WP_MAX_MEMORY_LIMIT' => [
				'label' => 'WP_MAX_MEMORY_LIMIT',
				'value' => WP_MAX_MEMORY_LIMIT,
			],
			'DISABLE_WP_CRON'     => [
				'label' => 'DISABLE_WP_CRON',
				'value' => $disable_wp_cron,
				'debug' => $disable_wp_cron_debug,
			],
			'WP_DEBUG'            => [
				'label' => 'WP_DEBUG',
				'value' => WP_DEBUG ? 'Enabled' : 'Disabled',
				'debug' => WP_DEBUG,
			],
			'WP_DEBUG_DISPLAY'    => [
				'label' => 'WP_DEBUG_DISPLAY',
				'value' => WP_DEBUG_DISPLAY ? 'Enabled' : 'Disabled',
				'debug' => WP_DEBUG_DISPLAY,
			],
			'WP_DEBUG_LOG'        => [
				'label' => 'WP_DEBUG_LOG',
				'value' => $wp_debug_log_value,
				'debug' => WP_DEBUG_LOG,
			],
			'SCRIPT_DEBUG'        => [
				'label' => 'SCRIPT_DEBUG',
				'value' => SCRIPT_DEBUG ? 'Enabled' : 'Disabled',
				'debug' => SCRIPT_DEBUG,
			],
			'WP_CACHE'            => [
				'label' => 'WP_CACHE',
				'value' => WP_CACHE ? 'Enabled' : 'Disabled',
				'debug' => WP_CACHE,
			],
			'CONCATENATE_SCRIPTS' => [
				'label' => 'CONCATENATE_SCRIPTS',
				'value' => $concatenate_scripts,
				'debug' => $concatenate_scripts_debug,
			],
			'COMPRESS_SCRIPTS'    => [
				'label' => 'COMPRESS_SCRIPTS',
				'value' => $compress_scripts,
				'debug' => $compress_scripts_debug,
			],
			'COMPRESS_CSS'        => [
				'label' => 'COMPRESS_CSS',
				'value' => $compress_css,
				'debug' => $compress_css_debug,
			],
			'WP_LOCAL_DEV'        => [
				'label' => 'WP_LOCAL_DEV',
				'value' => $wp_local_dev,
				'debug' => $wp_local_dev_debug,
			],
			'DB_CHARSET'          => [
				'label' => 'DB_CHARSET',
				'value' => ( defined( 'DB_CHARSET' ) ? DB_CHARSET : 'Undefined' ),
				'debug' => ( defined( 'DB_CHARSET' ) ? DB_CHARSET : 'undefined' ),
			],
			'DB_COLLATE'          => [
				'label' => 'DB_COLLATE',
				'value' => ( defined( 'DB_COLLATE' ) ? DB_COLLATE : 'Undefined' ),
				'debug' => ( defined( 'DB_COLLATE' ) ? DB_COLLATE : 'undefined' ),
			],
		];
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-paths
		|--------------------------------------------------------------------------
		*/
		
		$info['wp-paths']['fields'] = [
			'wordpress_path'  => [
				'label' => __( 'WordPress directory location' ),
				'value' => untrailingslashit( ABSPATH ),
			],
			'wp_content_path' => [
				'label' => __( 'wp-content directory location' ),
				'value' => WP_CONTENT_DIR,
			],
			'uploads_path'    => [
				'label' => __( 'Uploads directory location' ),
				'value' => $upload_dir['basedir'],
			],
			'plugins_path'    => [
				'label' => __( 'Plugins directory location' ),
				'value' => WP_PLUGIN_DIR,
			],
			'themes_path'     => [
				'label' => __( 'Themes directory location' ),
				'value' => get_theme_root(),
			],
		
		];
		
		/*
		|--------------------------------------------------------------------------
		| Section: wp-filesystem
		|--------------------------------------------------------------------------
		*/
		
		$upload_dir = wp_upload_dir();
		
		$is_writable_abspath            = wp_is_writable( ABSPATH );
		$is_writable_wp_content_dir     = wp_is_writable( WP_CONTENT_DIR );
		$is_writable_upload_dir         = wp_is_writable( $upload_dir['basedir'] );
		$is_writable_wp_plugin_dir      = wp_is_writable( WP_PLUGIN_DIR );
		$is_writable_template_directory = wp_is_writable( get_template_directory() . '/..' );
		
		$info['wp-filesystem']['fields'] = [
			'wordpress'  => [
				'label' => 'The main WordPress directory',
				'value' => ( $is_writable_abspath ? 'Writable' : 'Not writable' ),
				'debug' => ( $is_writable_abspath ? 'writable' : 'not writable' ),
			],
			'wp-content' => [
				'label' => 'The wp-content directory',
				'value' => ( $is_writable_wp_content_dir ? 'Writable' : 'Not writable' ),
				'debug' => ( $is_writable_wp_content_dir ? 'writable' : 'not writable' ),
			],
			'uploads'    => [
				'label' => 'The uploads directory',
				'value' => ( $is_writable_upload_dir ? 'Writable' : 'Not writable' ),
				'debug' => ( $is_writable_upload_dir ? 'writable' : 'not writable' ),
			],
			'plugins'    => [
				'label' => 'The plugins directory',
				'value' => ( $is_writable_wp_plugin_dir ? 'Writable' : 'Not writable' ),
				'debug' => ( $is_writable_wp_plugin_dir ? 'writable' : 'not writable' ),
			],
			'themes'     => [
				'label' => 'The themes directory',
				'value' => ( $is_writable_template_directory ? 'Writable' : 'Not writable' ),
				'debug' => ( $is_writable_template_directory ? 'writable' : 'not writable' ),
			],
		];
		
		return $info;
	}
}