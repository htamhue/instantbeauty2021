<?php

namespace AdScale\Handlers;

class CachingBehavior {
	
	//Wp Rocket: exclude from minification/concatenation
	public static function wpRocketExcludeJs( $excluded_js ) {
		$excluded_js[] = 'adscale.com';
		
		return $excluded_js;
	}
}
