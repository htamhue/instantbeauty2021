<?php
//  don't load directly
defined( 'ABSPATH' ) || exit;
?>


<div class="wrap">
	
	<?php do_action( 'adscale/before_settings_page' ); ?>
	
	<h2><?php _e( 'AdScale settings', 'adscale' ); ?></h2>
	
	<form method="post" action="options.php" id="wc_plugin_adscale_settings_form">
		<?php
		do_action( 'adscale/before_settings_form' );
		?>
		
		<br><br><br>
		<a href="<?php echo esc_url( \AdScale\Handlers\GoToAdscale::getEnableAdscaleBtnUrl() ) ?>"
		   class="button-primary" target="_blank">
			<?php esc_html_e( 'GO TO ADSCALE', 'adscale' ) ?>
		</a>
		<br><br>
		
		<?php
		settings_fields( 'wc_plugin_adscale_settings__main_group' );
		do_settings_sections( 'wc_plugin_adscale_settings' );
		//submit_button();
		do_action( 'adscale/after_settings_form' );
		?>
	</form>
	
	<?php do_action( 'adscale/after_settings_page' ); ?>

</div>
