<?php

return [
	
	'gsv_code_request_tpl' => 'https://ecommerce-scripts.adscale.com/ecommerce/script/%SHOP_DOMAIN%/metatag',
	
	'adscale_script_url_base' => 'https://storage-pu.adscale.com/static/ecom_js/%SHOP_DOMAIN%/adscale_purchase.js',
	'adscale_proxy_url_base'  => 'https://ecommerce-scripts.adscale.com/EcommerceProxy',
	
	'adscale_script_handle' => 'adscale-ecommerce', // id
	
	'js_variables' => [
		'proxy_ajax_url'      => 'adscale_proxy_ajax_url',
		'cart_ajax_url'       => 'adscale_ajax_url',
		'last_order_ajax_url' => 'adscale_ajax_order_url',
		'product_id'          => 'adscale_product_id',
		'product_value'       => 'adscale_product_value',
		'order_id'            => 'adscale_order_id',
		'order_value'         => 'adscale_order_value',
		'order_currency'      => 'adscale_order_currency',
	],
	
	'heartbeat' => [
		'request_tpl' => 'https://ecommerce-scripts.adscale.com/ecommerce/script/%SHOP_DOMAIN%/heartbeat',
		'interval'    => HOUR_IN_SECONDS * 6 // sec
	],
	
	'module_events' => [
		'module_enable_started' => [
			'request_tpl' => 'https://tools.adscale.com/EcommerceEvent?stage=%EVENT%&email=%EMAIL%&platform=%PLATFORM%&shop_host=%SHOP_DOMAIN%',
			'event_name'  => 'PluginInstallationStarted',
		],
		'module_enabled'        => [
			'request_tpl' => 'https://tools.adscale.com/EcommerceEvent?stage=%EVENT%&email=%EMAIL%&platform=%PLATFORM%&shop_host=%SHOP_DOMAIN%',
			'event_name'  => 'PluginInstalled',
		],
		'module_disable'        => [
			'request_tpl' => 'https://app.adscale.com/EcommerceEventListner?shopHost=%SHOP_DOMAIN%&event=%EVENT%',
			'event_name'  => 'module_disable',
		],
		'module_uninstall'      => [
			'request_tpl' => 'https://app.adscale.com/EcommerceEventListner?shopHost=%SHOP_DOMAIN%&event=%EVENT%',
			'event_name'  => 'module_uninstall',
		],
	],
	
	'logger' => [
		'enabled'            => true,
		'dir'                => wp_get_upload_dir()['basedir'] . '/adscale',
		'time_in_ms'         => true,
		'need_deny_htaccess' => true,
	],
	
	'shop' => [
		'keys_creation_params'      => [
			'app_name'    => 'AdScale App',
			'app_user_id' => 'adscale_app',
			'scope'       => 'read', //'read', 'write', 'read_write'
		],
		'keys_creation_user'        => [
			'login' => 'adscale_app',
			'name'  => 'AdScale App',
			'email' => 'support@adscale.com',
			'role'  => 'shop_manager',
		],
		'platform'                  => 'woo',
		'wc_legacy_api_auto_enable' => true,
	],
	
	'default_consider_pending_orders' => true,
];
