<?php

namespace AdScale\API;

use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;


class API_Manager {
	
	
	/**
	 * Get google_site_verification from API and save it
	 *
	 * @return bool|string
	 */
	public static function process_api_gsv_code() {
		
		$code = self::get_api_gsv_code();
		
		if ( $code ) {
			self::save_api_gsv_code( $code );
			
			return $code;
		}
		
		return false;
	}
	
	
	
	
	public static function get_api_gsv_code() {
		
		if ( ! $request_template = Helper::getConfigSetting( 'gsv_code_request_tpl', '' ) ) {
			return false;
		}
		
		$request_url = str_replace( '%SHOP_DOMAIN%', Helper::getShopDomain(), $request_template );
		
		$result = API_Process::process( $request_url, 'gsv_code' );
		
		$is_success = ! empty( $result['data']['success'] ) && $result['data']['success'] === '1';
		$code       = ! empty( $result['data']['tag'] ) ? $result['data']['tag'] : '';
		
		if ( $is_success && $code ) {
			return $code;
		}
		
		return false;
	}
	
	
	
	
	public static function save_api_gsv_code( $code ) {
		update_option( 'adscale__gsv', $code );
	}
	
	
	
	
	public static function get_saved_api_gsv_code() {
		return get_option( 'adscale__gsv', '' );
	}
	
	
	
	
	public static function process_heartbeat() {
		
		if ( ! $request_template = Helper::getConfigSetting( 'heartbeat/request_tpl', '' ) ) {
			return false;
		}
		
		$request_url = str_replace( '%SHOP_DOMAIN%', Helper::getShopDomain(), $request_template );
		$result      = API_Process::process( $request_url, 'process_heartbeat' );
		$is_success  = ! empty( $result['data']['success'] ) && $result['data']['success'] === '1';
		Logger::log( $is_success, 'is_success: ', 'process_heartbeat' );
		
		return $is_success;
	}
	
	
	
	
	public static function process_proxy() {
		$params              = $_GET;
		$params['shop_host'] = Helper::getShopDomain();
		
		if ( ( ! $proxyEndpoint = Helper::getProxyEndpoint() ) || count( $params ) >= 100 ) {
			return false;
		}
		
		$request_url = $proxyEndpoint . '?' . http_build_query( $params, null, '&', PHP_QUERY_RFC3986 );
		$result      = API_Process::process( $request_url, 'processProxy', false );
		
		return $result['data_raw'];
	}
	
	
	
	
	public static function process_module_event( $request_template, $request_event_name ) {
		
		if ( ! $request_template || ! $request_event_name ) {
			return false;
		}
		
		$request_url = str_replace(
			[
				'%SHOP_DOMAIN%',
				'%EVENT%',
				'%PLATFORM%',
				'%EMAIL%',
			],
			[
				Helper::getShopDomain(),
				$request_event_name,
				Helper::getConfigSetting( 'shop/platform', 'woo' ),
				Helper::getResolvedActivationEmail(),
			],
			$request_template
		);
		$result      = API_Process::process( $request_url, 'Events' );
		$is_success  = ! empty( $result['success'] ) && $result['success'] === true;
		
		return $is_success;
	}
	
	
}
