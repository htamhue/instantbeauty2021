<?php

namespace AdScale\API;


use AdScale\Exception\AdScaleExeption;
use AdScale\Helpers\Logger;

class API_Process {
	
	
	public static $err_codes = [
		777100 => 'ERR__CURL_ERROR',
		777200 => 'ERR__RESPONSE_HTTP_ERROR',
	];
	
	
	
	
	public static function process( $request_url, $log_name = 'common', $check_code_200 = true ) {
		
		try {
			// ------------------------------------------------------------------
			$request = new API_Request();
			
			$options = [
				CURLOPT_URL        => $request_url,
				CURLOPT_HTTPHEADER => [],
			];
			
			$request->init( $options );
			$res = $request->exec();
			
			
			// error cURL
			if ( $res['curl_error'] ) {
				throw new AdScaleExeption(
					$res['curl_error'],
					self::get_error_code( 'ERR__CURL_ERROR' ),
					null,
					__( 'error cURL ', 'adscale' )
				);
			}
			
			// error 
			if ( $res['http_code'] !== 200 && $check_code_200 ) {
				
				throw new AdScaleExeption(
					"HTTP Code = {$res['http_code']}",
					self::get_error_code( 'ERR__RESPONSE_HTTP_ERROR' ),
					null,
					__( 'response http code error', 'adscale' )
				);
			}
			
			// ------------------------------------------------------------------
			
		} catch ( AdScaleExeption $e ) {
			
			Logger::log( "ERROR [request_url({$request_url})] [{$e->getCode()}|" . self::get_error_name( $e->getCode() ) . "] : {$e->getMessage()}", 'API:', $log_name );
			
			return [
				'success'       => false,
				'message'       => "API: ERROR [{$e->getCode()}|" . self::get_error_name( $e->getCode() ) . "] : {$e->getMessage()}",
				'message_front' => $e->getMessageFront(),
				'error_code'    => $e->getCode(),
				'data'          => [],
				'data_raw'      => '',
			];
		}
		
		// http_code === 200 OK
		
		Logger::log( "SUCCESS [request_url({$request_url})]", 'API:', $log_name );
		
		return [
			'success'       => true,
			'message'       => '',
			'message_front' => '',
			'error_code'    => 0,
			'data'          => $res['body'],
			'data_raw'      => $res['body_raw'],
		];
	}
	
	
	// ==========================================================================
	
	
	
	public static function get_error_code( $error_name ) {
		return array_search( $error_name, self::$err_codes, true );
	}
	
	
	
	public static function get_error_name( $error_code ) {
		return ! empty( self::$err_codes[ $error_code ] ) ? self::$err_codes[ $error_code ] : false;
	}
	
	
}
