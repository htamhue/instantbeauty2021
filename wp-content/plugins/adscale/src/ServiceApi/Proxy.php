<?php

namespace AdScale\ServiceApi;

use AdScale\API\API_Manager;
use AdScale\Helpers\Helper;


class Proxy extends ServiceApiBase {
	
	public static function initContent() {
		global $wp;
		
		if ( isset( $_GET['adscaleProxy'] ) ) {
			$wp->query_vars['adscaleProxy'] = sanitize_key( wp_unslash( $_GET['adscaleProxy'] ) );
		}
		
		//  make sure endpoint request
		if ( ! isset( $wp->query_vars['adscaleProxy'] ) ) {
			return;
		}
		
		
		try {
			Helper::nocacheHeaders();
			
			// Handle Request
			self::handleRequest();
			
			// Default error
			Helper::sendResponseJsonError( self::makeErrorBody() );
		} catch ( ServiceApiExeption $ex ) {
			Helper::sendResponseJsonError(
				self::makeErrorBody( $ex->getApiErrorCode(), $ex->getMessage() )
			);
		} catch ( \Exception $ex ) {
			Helper::sendResponseJsonError( self::makeErrorBody( $ex->getCode(), $ex->getMessage() ) );
		}
	}
	
	
	public static function handleRequest() {
		$response = API_Manager::process_proxy();
		Helper::sendResponse( $response, 200, '', 'application/json' );
	}
}
