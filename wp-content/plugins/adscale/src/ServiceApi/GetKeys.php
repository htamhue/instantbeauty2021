<?php

namespace AdScale\ServiceApi;

use AdScale\Handlers\ShopKeys;
use AdScale\Helpers\Helper;


class GetKeys extends ServiceApiBase {
	
	public static function initContent() {
		global $wp;
		
		if ( isset( $_GET['adscaleGetKeys'] ) ) {
			$wp->query_vars['adscaleGetKeys'] = sanitize_key( wp_unslash( $_GET['adscaleGetKeys'] ) );
		}
		
		//  make sure endpoint request
		if ( ! isset( $wp->query_vars['adscaleGetKeys'] ) ) {
			return;
		}
		
		
		try {
			ob_start();
			
			// Error handler
			self::setErrorHandler();
			
			Helper::nocacheHeaders();
			
			// Check Request (Guards)
			self::checkRequest();
			
			ob_end_clean();
			
			// Handle Request
			self::handleRequest();
			
			// Default error
			Helper::sendResponseFormattedError( self::makeErrorBody() );
		} catch ( ServiceApiExeption $ex ) {
			Helper::sendResponseFormattedError(
				self::makeErrorBody( $ex->getApiErrorCode(), $ex->getMessage() )
			);
		} catch ( \Throwable $t ) {
			// Executed only in PHP 7, will not match in PHP 5
			$message = "{$t->getMessage()} Trace: {$t->getTraceAsString()}";
			Helper::sendResponseFormattedError( self::makeErrorBody( $t->getCode(), $message ) );
		} catch ( \Exception $ex ) {
			$message = "{$ex->getMessage()} Trace: {$ex->getTraceAsString()}";
			Helper::sendResponseFormattedError( self::makeErrorBody( $ex->getCode(), $message ) );
		}
	}
	
	
	
	/**
	 * @throws ServiceApiExeption
	 */
	public static function handleRequest() {
		$keys_data       = ShopKeys::get_keys_data();
		$key_id          = $keys_data['key_id'];
		$consumer_key    = $keys_data['consumer_key'];
		$consumer_secret = $keys_data['consumer_secret'];
		$shop_host       = Helper::getShopDomain();
		$platform        = Helper::getConfigSetting( 'shop/platform', 'woo' );
		
		if ( ! $consumer_key ) {
			throw new ServiceApiExeption( 'System error: $consumer_key is empty', '000' );
		}
		
		if ( ! $consumer_secret ) {
			throw new ServiceApiExeption( 'System error: $consumer_secret is empty', '000' );
		}
		
		if ( ! $shop_host ) {
			throw new ServiceApiExeption( 'System error: $shop_host is empty', '000' );
		}
		
		if ( ! $platform ) {
			throw new ServiceApiExeption( 'System error: $platform is empty', '000' );
		}
		
		Helper::sendResponseFormattedSuccess( [
			'shop_host'       => $shop_host,
			'platform'        => $platform,
			'consumer_key'    => $consumer_key,
			'consumer_secret' => $consumer_secret,
			'version'         => ADSCALE_INTERNAL_MODULE_VERSION,
		] );
	}
}
