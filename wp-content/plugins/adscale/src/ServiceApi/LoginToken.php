<?php

namespace AdScale\ServiceApi;

use AdScale\Helpers\Helper;
use AdScale\Handlers\LoginToken as LoginTokenHandler;

class LoginToken extends ServiceApiBase {
	
	public static function initContent() {
		global $wp;
		
		if ( isset( $_GET['adscaleLoginToken'] ) ) {
			$wp->query_vars['adscaleLoginToken'] = sanitize_key( wp_unslash( $_GET['adscaleLoginToken'] ) );
		}
		
		//  make sure endpoint request
		if ( ! isset( $wp->query_vars['adscaleLoginToken'] ) ) {
			return;
		}
		
		
		try {
			ob_start();
			
			Helper::nocacheHeaders();
			
			// Check Request (Guards)
			self::checkRequest();
			
			ob_end_clean();
			
			// Handle Request
			self::handleRequest();
			
			// Default error
			Helper::sendResponseFormattedError( self::makeErrorBody() );
		} catch ( ServiceApiExeption $ex ) {
			Helper::sendResponseFormattedError(
				self::makeErrorBody( $ex->getApiErrorCode(), $ex->getMessage() )
			);
		} catch ( \Exception $ex ) {
			Helper::sendResponseFormattedError( self::makeErrorBody( $ex->getCode(), $ex->getMessage() ) );
		}
	}
	
	public static function handleRequest() {
		$loginToken = LoginTokenHandler::getLoginToken();
		//$isEmptyLT  = empty($loginToken) ? 'empty' : 'not empty';
		//Logger::log("process loginToken: OK [$isEmptyLT]", 'Success > ', 'process_loginToken');
		LoginTokenHandler::clearLoginToken();  // clear after getting
		// body == {"token":"%TOKEN%"}
		Helper::sendResponseFormattedSuccess( [ 'token' => $loginToken ] );
	}
}
