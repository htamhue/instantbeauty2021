<?php

namespace AdScale\ServiceApi;

use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;


class LastOrder extends ServiceApiBase {
	
	public static function initContent() {
		global $wp;
		
		if ( isset( $_GET['adscaleLastOrder'] ) ) {
			$wp->query_vars['adscaleLastOrder'] = sanitize_key( wp_unslash( $_GET['adscaleLastOrder'] ) );
		}
		
		//  make sure endpoint request
		if ( ! isset( $wp->query_vars['adscaleLastOrder'] ) ) {
			return;
		}
		
		
		try {
			ob_start();
			
			Helper::nocacheHeaders();
			
			// Check Request (Guards)
			self::checkRequest();
			
			ob_end_clean();
			
			// Handle Request
			self::handleRequest();
			
			// Default error
			Helper::sendResponseJsonError( self::makeErrorBody() );
		} catch ( ServiceApiExeption $ex ) {
			Helper::sendResponseJsonError(
				self::makeErrorBody( $ex->getApiErrorCode(), $ex->getMessage() )
			);
		} catch ( \Exception $ex ) {
			Helper::sendResponseJsonError( self::makeErrorBody( $ex->getCode(), $ex->getMessage() ) );
		}
	}
	
	
	/**
	 * The set of checks
	 *
	 * @throws ServiceApiExeption
	 */
	public static function checkRequest() {
		// Wrong HTTP Method guard
		if ( ! self::checkHTTPMethod() ) {
			throw new ServiceApiExeption( 'System error: HTTP Method not supported', '000' );
		}
	}
	
	
	public static function handleRequest() {
		$response = self::getLastOrderSimplified();
		Helper::sendResponseJsonSuccess( $response );
	}
	
	
	public static function getLastOrderSimplified() {
		
		$order_id       = self::getLastOrderIdFromCookie();
		$order_value    = Helper::get_order_value( $order_id );
		$order          = function_exists( 'wc_get_order' ) ? wc_get_order( $order_id ) : false;
		$order_currency = is_object( $order ) ? $order->get_currency() : null;
		
		return [
			'order_id'       => $order_id ?: '',
			'order_value'    => is_numeric( $order_value ) ? $order_value : '',
			'order_currency' => $order_currency ?: '',
		];
	}
	
	
	public static function getLastOrderIdFromCookie() {
		return (int) Helper::getArrayValue( $_COOKIE, 'adscale__last_order_id' );
	}
}
