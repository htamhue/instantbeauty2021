<?php
/*
Plugin Name: AdScale
Description: AdScale plugin allows you to automate Ecommerce advertising across all channels and drive more sales to your store. Easily create, manage & optimize ads on one platform.
Version: 1.6.1
Author: AdScale LTD
*/

use AdScale\App;

//  don't load directly
defined( 'ABSPATH' ) || exit;

define( 'ADSCALE_INTERNAL_MODULE_VERSION', 'v20201002' );
define( 'ADSCALE_PLUGIN_DIR', __DIR__ );
define( 'ADSCALE_PLUGIN_FILE', __FILE__ );
define( 'ADSCALE_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

//  helper functions for developers
require_once ADSCALE_PLUGIN_DIR . '/dev.php';


// autoload
require_once ADSCALE_PLUGIN_DIR . '/autoload.php';


$app    = App::instance();
$config = apply_filters( 'adscale/config', require ADSCALE_PLUGIN_DIR . '/src/config/config.php' );

try {
	$app->run( $config );
} catch ( Exception $exception ) {
	wlog( $exception );
	header( 'HTTP/1.1 503 Service Temporarily Unavailable' );
	header( 'Status: 503 Service Temporarily Unavailable' );
	header( 'Retry-After: 300' );// 300 seconds.
	die();
}