<?php

/**
 * Fired when the plugin is uninstalled.
 *
 */

use AdScale\Handlers\Events;
use AdScale\Handlers\ShopKeys;
use AdScale\Helpers\Helper;
use AdScale\Helpers\Logger;

// If uninstall not called from WordPress, then exit.

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit;
}


define( 'ADSCALE_PLUGIN_DIR', __DIR__ );

// autoload
require_once ADSCALE_PLUGIN_DIR . '/autoload.php';

// set logger config
Logger::setConfig( Helper::getConfigSetting( 'logger', '[]', true ) );

// delete api user
$user_login = sanitize_user( Helper::getConfigSetting( 'shop/keys_creation_user/login', 'adscale_app', true ) );
/** @var  $User \WP_User */
$User    = get_user_by( 'login', $user_login );
$user_id = 0;
if ( $User instanceof \WP_User && $user_id = (int) $User->ID ) {
	wp_delete_user( $user_id );
}


function adscale_unistall_delete_data( $user_id ) {
	// Send uninstall event to AdScale
	Events::moduleUninstall();
	
	// delete api key
	if ( $user_id ) {
		ShopKeys::delete_shop_keys_data_by_user_id( $user_id );
	}
	
	// delete all plugin options if exists
	delete_option( 'adscale__shop_domain' );
	delete_option( 'adscale__gsv' );
	delete_option( 'adscale__gsv_from_api' ); // bc
	delete_option( 'adscale__access_key_id' );
	delete_option( 'adscale__access_consumer_key' );
	delete_option( 'adscale__access_consumer_secret' );
	delete_option( 'adscale__access_wp_user_id' );
	delete_option( 'adscale__need_activation_redirect' );
	delete_option( 'adscale__login_token' );
	delete_option( 'adscale__proxy_endpoint' );
	delete_option( 'adscale__consider_pending_orders' );
}




if ( is_multisite() ) {
	$sites = get_sites();
	
	foreach ( $sites as $site ) {
		switch_to_blog( $site->blog_id );
		adscale_unistall_delete_data( $user_id );
		restore_current_blog();
	}
} else {
	adscale_unistall_delete_data( $user_id );
}
