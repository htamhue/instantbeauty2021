<?php
/**
 * Display single product reviews (comments)
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product-reviews.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

global $product;

if ( ! comments_open() ) {
	return;
}

?>
<div id="reviews" class="woocommerce-Reviews">
	<div id="comments">
		<h2 class="woocommerce-Reviews-title">
			<?php
			$count = $product->get_review_count();
			if ( $count && wc_review_ratings_enabled() ) {
				/* translators: 1: reviews count 2: product name */
				$reviews_title = sprintf( esc_html( _n( '%1$s review for %2$s', '%1$s reviews for %2$s', $count, 'woocommerce' ) ), esc_html( $count ), '<span>' . get_the_title() . '</span>' );
				echo apply_filters( 'woocommerce_reviews_title', $reviews_title, $count, $product ); // WPCS: XSS ok.
			} else {
				esc_html_e( 'Reviews', 'woocommerce' );
			}
			?>
		</h2>
<div id="amazon-comments">
<a href="https://amzn.to/35uXtUT" target="_blank">Link to Amazon's reviews</a>
</div>
		<?php /* if ( have_comments() ) : ?>
			<ol class="commentlist">
				<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>

			<?php
			if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				echo '<nav class="woocommerce-pagination">';
				paginate_comments_links(
					apply_filters(
						'woocommerce_comment_pagination_args',
						array(
							'prev_text' => '&larr;',
							'next_text' => '&rarr;',
							'type'      => 'list',
						)
					)
				);
				echo '</nav>';
			endif;

			?>
		<?php else : ?>
			<p class="woocommerce-noreviews"><?php esc_html_e( 'There are no reviews yet.', 'woocommerce' ); ?></p>
		<?php endif; */ ?>

<!-- comment List by ajax  -->
<?php
	$count = 0;
	$comment_post_id = get_the_ID();
 	$all_comments = $wpdb->get_results("SELECT wpc.comment_id, wpc.comment_author,wpc.comment_author_email,wpc.comment_date,wpc.comment_content,wpcm.meta_value AS rating, wpcmv.meta_value as verified  FROM `" . $wpdb->prefix . "comments` AS wpc INNER JOIN  `" . $wpdb->prefix . "commentmeta` AS wpcm ON wpcm.comment_id = wpc.comment_id AND wpcm.meta_key = 'rating' LEFT JOIN `wplp_commentmeta` AS wpcmv ON wpcmv.comment_id = wpc.comment_id AND wpcmv.meta_key = 'verified' WHERE wpc.comment_post_id = '".$comment_post_id."' AND comment_approved=1 ORDER BY FIELD(verified,1) DESC,wpc.comment_id ASC limit 0, 10 ");

        $count = $wpdb->get_results("SELECT count(wpc.comment_ID) as total_count FROM `" . $wpdb->prefix . "comments` AS wpc INNER JOIN  `" . $wpdb->prefix . "commentmeta` AS wpcm ON wpcm.comment_id = wpc.comment_id AND wpcm.meta_key = 'rating' LEFT JOIN `wplp_commentmeta` AS wpcmv ON wpcmv.comment_id = wpc.comment_id AND wpcmv.meta_key = 'verified' WHERE wpc.comment_post_id = '".$comment_post_id."' AND comment_approved=1 ");
       
         $count = $count[0]->total_count;

        // Loop into all the comments
        $pag_container = '';
            foreach($all_comments as $key => $comment):

	        	$comments_meta = $wpdb->get_results("SELECT wpcm.meta_id, wpcm.comment_id,wpcm.meta_key,wpcm.meta_value FROM `" . $wpdb->prefix . "commentmeta` AS wpcm WHERE  comment_id = '".$comment->comment_id."' ");
	        	$star_width ='0%';
	        	$star_width = (20 * $comment->rating).'%';
	           	$verified = 0; $ivole_order = 0; $ivole_review_image ='';
	           	foreach($comments_meta as $key_meta => $meta):
	           			if($meta->meta_key =='verified'){
	           				$verified = $meta->meta_value;
	           			}
	           			if($meta->meta_key =='ivole_review_image'){
	           				$ivole_review_image = unserialize($meta->meta_value);
		           			$ivole_review_image = $ivole_review_image['url'];		
	           			}
	           			if($meta->meta_key =='ivole_order'){
	           				$ivole_order = 	$meta->meta_value;
	           			}
	           	endforeach;	
	            // Set the desired output into a variable
	            $msg .= '
	            <li class="review even thread-even depth-1 li-comment" id="li-comment-'.$comment->comment_id.'">
					<div id="comment-'.$comment->comment_id.'" class="comment_container">
						<div class="comment-text">
							<div class="star-rating" role="img" aria-label="Rated '.$comment->rating.' out of 5"><span style="width:'.$star_width.'">Rated <strong class="rating">'.$comment->rating.'</strong> out of 5</span></div>
							<p class="meta">
								<strong class="woocommerce-review__author">'.$comment->comment_author.' </strong>
								<span class="woocommerce-review__dash">&ndash;</span> 
								<time class="woocommerce-review__published-date" datetime="'.$comment->comment_date.'">'.$comment->comment_date.'</time>
							</p> ';
					if($verified > 0){
						$msg .= '	
							<p class="ivole-verified-badge">
								<img src="'.site_url().'/wp-content/plugins/customer-reviews-woocommerce/img/shield-20.png" alt="Verified review" class="ivole-verified-badge-icon">
								<span class="ivole-verified-badge-text">Verified review - 
									<a href="https://www.cusrev.com/reviews/instant-beauty.co.uk/p/p-'.$comment_post_id.'/r-'.$ivole_order.'" title="" target="_blank" rel="nofollow noopener">view original</a>
									<img src="'.site_url().'/wp-content/plugins/customer-reviews-woocommerce/img/external-link.png" alt="External link" class="ivole-verified-badge-ext-icon">
								</span>
							</p> ';
					}			
				$msg .= '	<div class="description">
								<p>'.$comment->comment_content.'</p> ';

					if($ivole_review_image !=''){
						$msg .= '
							<p class="iv-comment-image-text">Uploaded image(s):</p>
						<div class="iv-comment-images">
							<div class="iv-comment-image">
								<a href="'.$ivole_review_image.'" class="ivole-comment-a" rel="nofollow">
									<img src="'.$ivole_review_image.'" alt="Image #1 from '.$comment->comment_author.' "/>
								</a>
							</div>
							<div style="clear:both;">
							</div>
						</div> ';
					}				

				$msg .= '	</div>
							<span class="ivole-voting-cont">
							<span id="ivole-reviewvoting-'.$comment->comment_id.'">Was this review helpful to you?</span><span class="ivobe-letter-space"></span><span class="ivole-declarative"><div class="ivole-vote-button-margin"><span class="ivole-a-button"><span class="ivole-a-button-inner"><a id="ivole-reviewyes-'.$comment->comment_id.'" class="ivole-a-button-text" href="#"><div class="ivole-vote-button">Yes</div></a></span></span></div></span><span class="ivobe-letter-space"></span><span class="ivole-declarative"><div class="ivole-vote-button-margin"><span class="ivole-a-button"><span class="ivole-a-button-inner"><a id="ivole-reviewno-'.$comment->comment_id.'" class="ivole-a-button-text" href="#"><div class="ivole-vote-button">No</div></a></span></span></div></span><span class="ivobe-letter-space"></span></span>
						</div>
					</div>	
				</li>';
	        endforeach;
        // Optional, wrap the output into a container
        //$msg = "<ol class='commentlist'>" . $msg . "</ol>";
?>
<ol class='commentlist'>
	<div class="post">
		<?= $msg ?>
	</div>
</ol>
<?php if($count > 10){ ?>           
	<button class="load-more-review-comment" style="cursor:pointer;margin-bottom:7px;background: black;color:white;padding:7px 8px;border:1px solid black;">Load More</button>
<?php } ?>
<input type="hidden" id="row" value="0">
<input type="hidden" id="all" value="<?= $count ?>">

<script type="text/javascript">
//jQuery(document).ready(function(){
    jQuery('.load-more-review-comment').click(function(e){
    	var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        var row = Number(jQuery('#row').val());
        var allcount = Number(jQuery('#all').val());
        var rowperpage = 10;
        row = row + rowperpage;
        if(row <= allcount){
            jQuery("#row").val(row);
            jQuery.ajax({
                url: ajaxurl,
                type: 'post',
                //data: {row:row},
                data : {
					'action': 'pagination-load-more-comment', // wp_ajax_cloadmore
					comment_post_id: <?= get_the_ID(); ?>,  // the current post
					'page' : row, // current comment page
				},
                //beforeSend:function(){
                beforeSend : function ( xhr ) {	
                    jQuery(".load-more-review-comment").text("Loading...");
                    jQuery('.load-more-review-comment').css("cursor","none");
                },
                success: function(response){
                    // Setting little delay while displaying new content
                    setTimeout(function() {
                        // appending posts after last post with class="post"
                        jQuery(".post:last").after(response).show().fadeIn("slow");
                        var rowno = row + rowperpage;
                        // checking row value is greater than allcount or not
                        if(rowno > allcount){
                            // Change the text and background
                            //jQuery('.load-more-review-comment').text("Hide");
                            jQuery('.load-more-review-comment').hide();
                            jQuery('.load-more-review-comment').css("background","gray");
                        }else{
                            jQuery(".load-more-review-comment").text("Load more");
                            jQuery('.load-more-review-comment').css("cursor","pointer");
                        }
                    }, 2000);
                }
            });
        }else{
            jQuery('.load-more-review-comment').text("Loading...");
            jQuery('.load-more-review-comment').css("cursor","none");
            // Setting little delay while removing contents
            setTimeout(function() {
                // When row is greater than allcount then remove all class='post' element after 3 element
                jQuery('.post:nth-child(10)').nextAll('.post').remove();
                // Reset the value of row
                jQuery("#row").val(0);
                // Change the text and background
                //jQuery('.load-more-review-comment').text("Load more");
                jQuery('.load-more-review-comment').hide();
                jQuery('.load-more-review-comment').css("background","gray");
            }, 2000);
        }
    });
//});
</script>

<!-- end coment list by ajax -->

	</div>

	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>
		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
				$commenter = wp_get_current_commenter();

				$comment_form = array(
					/* translators: %s is product title */
					'title_reply'         => have_comments() ? __( 'Add a review', 'woocommerce' ) : sprintf( __( 'Be the first to review &ldquo;%s&rdquo;', 'woocommerce' ), get_the_title() ),
					/* translators: %s is product title */
					'title_reply_to'      => __( 'Leave a Reply to %s', 'woocommerce' ),
					'title_reply_before'  => '<span id="reply-title" class="comment-reply-title">',
					'title_reply_after'   => '</span>',
					'comment_notes_after' => '',
					'fields'              => array(
						'author' => '<p class="comment-form-author"><label for="author">' . esc_html__( 'Name', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label> ' .
									'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" required /></p>',
						'email'  => '<p class="comment-form-email"><label for="email">' . esc_html__( 'Email', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label> ' .
									'<input id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" required /></p>',
					),
					'label_submit'        => __( 'Submit', 'woocommerce' ),
					'logged_in_as'        => '',
					'comment_field'       => '',
				);

				$account_page_url = wc_get_page_permalink( 'myaccount' );
				if ( $account_page_url ) {
					/* translators: %s opening and closing link tags respectively */
					$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( esc_html__( 'You must be %1$slogged in%2$s to post a review.', 'woocommerce' ), '<a href="' . esc_url( $account_page_url ) . '">', '</a>' ) . '</p>';
				}

				if ( wc_review_ratings_enabled() ) {
					$comment_form['comment_field'] = '<div class="comment-form-rating"><label for="rating">' . esc_html__( 'Your rating', 'woocommerce' ) . '</label><select name="rating" id="rating" required>
						<option value="">' . esc_html__( 'Rate&hellip;', 'woocommerce' ) . '</option>
						<option value="5">' . esc_html__( 'Perfect', 'woocommerce' ) . '</option>
						<option value="4">' . esc_html__( 'Good', 'woocommerce' ) . '</option>
						<option value="3">' . esc_html__( 'Average', 'woocommerce' ) . '</option>
						<option value="2">' . esc_html__( 'Not that bad', 'woocommerce' ) . '</option>
						<option value="1">' . esc_html__( 'Very poor', 'woocommerce' ) . '</option>
					</select></div>';
				}

				$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . esc_html__( 'Your review', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label><textarea id="comment" name="comment" cols="45" rows="8" required></textarea></p>';

				comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>
	<?php else : ?>
		<p class="woocommerce-verification-required"><?php esc_html_e( 'Only logged in customers who have purchased this product may leave a review.', 'woocommerce' ); ?></p>
	<?php endif; ?>

	<div class="clear"></div>
</div>
