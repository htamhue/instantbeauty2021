jQuery(function($){	
	$('.checkout-custom-mod').hide();
	$('p.cancel-save').hide();

	//Checkout Trigger Edit Button
	$('p button.edit-cfm').click( function(){
		$("<div id='customer_details_new'></div>").insertAfter("#customer_details");
		$('#customer_details h3, #customer_details ul').clone().appendTo('#customer_details_new');

		$('#customer_details_new').hide();
		$('#customer_details').attr('id', 'customer_details_hide');
		$('#customer_details_hide').hide();
		$('.checkout-custom-mod').slideDown();
		$('p.cancel-save').show();
		$(this).hide();
		
	});
	
	//Checkout Trigger Cancel Edit Button
	$('p button.cncl-edit').click( function(){
		$('p button.edit-cfm').show();
		$('p.cancel-save, .checkout-custom-mod').hide();
		$('#customer_details_hide').attr('id', 'customer_details');
	});
	
	//Checkout Trigger sve-edit button - Print New Form Details
	
	$('p button.sve-edit').click(function(){
		//New shipping details
		var newShippingDetails = {
				firstName: $("#new_shipping_first_name").length ? $("#new_shipping_first_name").val() : '',
				lastName: $("#new_shipping_last_name").length ? $("#new_shipping_last_name").val() : '',
				country: $("#shipping_country").length ? $("#shipping_country option:selected").val() : '',
				state: $("[name='new_shipping_state']").length ? ($("[name='new_shipping_state']").is("select") ? $("[name='new_shipping_state'] option:selected").text() : $("[name='new_shipping_state']").val()) : '',
				city: $("#new_shipping_city").length ? $("#new_shipping_city").val() : '',
				firstAddress: $("#new_shipping_address_1").length ? $("#new_shipping_address_1").val() : '',
				secondAddress: $("#new_shipping_address_2").length ? $("#new_shipping_address_2").val() : '',
				zipcode: $("#new_shipping_postcode").length ? $("#new_shipping_postcode").val() : ''
			};	
			var newShippingAddress = '';
					
			//First Name & Last Name
			if ((newShippingDetails.firstName != '') || (newShippingDetails.lastName != '')) {
				newShippingAddress = newShippingDetails.firstName + ' ' + newShippingDetails.lastName + '<br>';
			}
			
	
			// First Address
			if (newShippingDetails.firstAddress != '') {
				newShippingAddress += newShippingDetails.firstAddress + '<br>';
			}
			
			// Second Address
			if (newShippingDetails.secondAddress != '') {
				newShippingAddress += newShippingDetails.secondAddress + '<br>';
			}
			
			// City
			if (newShippingDetails.city != '') {
				newShippingAddress += newShippingDetails.city + ', ';
			}
			
			// State
			if (newShippingDetails.state != '') {
				newShippingAddress += newShippingDetails.state + ' ';
			}
					//	console.log(newShippingDetails.state);

			// Zipcode
			if (newShippingDetails.zipcode != '') {
				newShippingAddress += newShippingDetails.zipcode;
			}
			
			// Country
			if (newShippingDetails.country != '') {
				newShippingAddress += '<br>' + newShippingDetails.country;
			}
			
			$('#customer_details_new').attr('id', 'customer_details');

			
			//Shipping Address						  
			$('#customer_details').append(newShippingAddress);			
			//$('#customer_details').show();
			

		$('#customer_details_hide').hide();
		
		$('p.cancel-save, .checkout-custom-mod').hide();
		$('p button.edit-cfm').show();
			
		console.log(newShippingAddress);
	});

	
});

jQuery(function($){
	$('#uap-avatar-button').on('click', function(){
		$('<span>SAVE</span>').append('i.cropControlCrop');
		
		});
		$('form#uap_createuser').find("input#uap_user_url_field").each(function(){
			
			 $(this).attr("placeholder", "Please add your website link");
		 //if(!$(this).val()) {  }
		 });
		jQuery(function($){
			
				$('select#uap_uap_country_field option:contains("United Kingdom (UK)")').prop('selected',true);
				
				if( $('select#uap_uap_country_field option:contains("United Kingdom (UK)")').prop('selected',true)){
				
					$('form#uap_createuser span#select2-uap_uap_country_field-container').removeAttr('title');
					$('form#uap_createuser span#select2-uap_uap_country_field-container').prop('title', 'United Kingdom (UK)');
					$('form#uap_createuser span#select2-uap_uap_country_field-container').html('United Kingdom (UK)');
				}
				else{
					retun;
				}
			
		 });
		$("<span class='tool-tip'><i class='fas fa-question'></i><p class='tool-tip-text'>App instruction: press on <i class='fas fa-bars'></i>, View your profile, press on More button, find Copy link to profile and paste it here</p> </span>").insertAfter("form#uap_createuser input#uap_socail_account_url_fb_field");
		
		$("<span class='tool-tip'><i class='fas fa-question'></i><p class='tool-tip-text'>App instruction: press on <i class='fas fa-user'></i>, View your profile, press on More button, find Copy link to profile and paste it here</p> </span>").insertAfter("form#uap_createuser input#uap_socail_account_url_insta_field");
		
		$("<span class='tool-tip'><i class='fas fa-question'></i><p class='tool-tip-text'>App instruction: press on <i class='fas fa-user'></i>, View your profile, press on More button, find Copy link to profile and paste it here</p> </span>").insertAfter("form#uap_createuser input#uap_socail_account_url_tiktok_field");
		
		$("<span class='tool-tip'><i class='fas fa-question'></i><p class='tool-tip-text'>Currently, we're paying commissions by PayPal only. Please enter your PayPal email address, to receive payment.</p> </span>").insertAfter("form#uap_createuser input#uap_uap_affiliate_paypal_email_field");

	
		var nothide = $('form#uap_createuser input#uap_user_login_field, form#uap_createuser input#uap_user_email_field, form#uap_createuser input#uap_first_name_field, form#uap_createuser input#uap_last_name_field, form#uap_createuser input#uap_pass1_field, input#uap_pass2_field,form#uap_createuser div.uap-strength-label,form#uap_createuser button#nextbtn').parent();
		
		$('<div class="custom_next"><button id="nextbtn">Next</button></div>').insertBefore('body.page-id-10028 form#uap_createuser > .uap-form-upload_image');

		$('form#uap_createuser div,form#uap_createuser label.uap-labels-register, form#uap_createuser label.uap-form-sublabel').not(nothide).addClass('hide');
		
		$('form#uap_createuser button#nextbtn').parent().removeClass('hide');
		$('form#uap_createuser input#uap_user_login_field, form#uap_createuser input#uap_first_name_field, input#uap_zip_field').parent().addClass('div-col-2 left');
		$('form#uap_createuser input#uap_user_email_field,form#uap_createuser input#uap_last_name_field, input#uap_city_field').parent().addClass('div-col-2 right');
		$('form#uap_createuser input#uap_county_uk_only_field').parent().addClass('div-col-2 left');
	
 

		$('form#uap_createuser #nextbtn').on('click', function(){
			$('input#uap_socail_account_url_insta_field, input#uap_socail_account_url_tiktok_field, input#uap_user_url_field').parent().hide();
			var step1 =  $(' input#uap_user_login_field, input#uap_user_email_field, input#uap_first_name_field, input#uap_last_name_field, input#uap_pass1_field, input#uap_pass2_field');
			var Fvalue =  $(' input#uap_user_login_field, input#uap_user_email_field, input#uap_first_name_field, input#uap_last_name_field, input#uap_pass1_field, input#uap_pass2_field').val();
			var step1Hide = $(' input#uap_user_login_field, input#uap_user_email_field, input#uap_first_name_field, input#uap_last_name_field, input#uap_pass1_field, input#uap_pass2_field').parent();
			
			if(Fvalue != ""){
			
				$('.uap-register-9 .uap-register-notice').hide();

				$(step1Hide).addClass('hide');
				$('form#uap_createuser div,label.uap-labels-register,  label.uap-form-sublabel').not(step1Hide).removeClass('hide');
				$('div.uap-tos-wrap, #uap_submit_bttn').show();
				$('#nextbtn').addClass('hide');
				
				$(document).ready(function(){
					$('input#uap_socail_account_url_field[type="checkbox"][value="Facebook"]').attr('checked', true);
					$('input#uap_socail_account_url_field[type="checkbox"][value="Facebook"]').attr('disabled', true);

					//$('input#uap_socail_account_url_field[type="checkbox"][value="Facebook"]').attr("required",true);
					$('input#uap_socail_account_url_fb_field').prop("required",true);
					$('input#uap_uap_optin_accept_field').attr('disabled', true);
					$('input#uap_uap_optin_accept_field').attr("checked",true);
					
					$('input#uap_uap_optin_accept_field').prop('required', true);
				});
				$('input#uap_socail_account_url_field[type="checkbox"][value="Facebook"]').on('click', function(){
					
				$('input#uap_socail_account_url_fb_field').parent().toggle();
				if(this.checked == false){
						$('input#uap_socail_account_url_fb_field').prop("required",false);
						
						$('input#uap_socail_account_url_fb_field').parent().hide();
					}
					
				});
				$('span.social-tool-tip').hover(function(){
					$('p.social-tool-tip-text').toggle();
				});
					
				
	 
			}
			
			
			
			$('input#uap_socail_account_url_field[type="checkbox"][value="Instagram"]').on('click', function(){
				$('input#uap_socail_account_url_insta_field').parent().toggle();

				//$('input#uap_socail_account_url_insta_field').show();
				$('input#uap_socail_account_url_insta_field').prop("required",true);
				if(this.checked == false){
					$('input#uap_socail_account_url_insta_field').prop("required",false);
				}
			}); 
			$('input#uap_socail_account_url_field[type="checkbox"][value="Tiktok"]').on('click', function(){
					$(' input#uap_socail_account_url_tiktok_field').parent().toggle();
					$(' input#uap_socail_account_url_tiktok_field').attr("required",true);

				
					if(this.checked == false){
						$('input#uap_socail_account_url_tiktok_field').prop("required",false);
						
					}
				});
			
			$('input#uap_socail_account_url_field[type="checkbox"][value="Website/Blog"]').on('click', function(){
				$('input#uap_user_url_field').parent().toggle();
				$('form#uap_createuser input#uap_user_url_field').parent().css("position", "unset");

				if(this.checked == true){
					
					$('input#uap_socail_account_url_fb_field').parent().hide();
					$(' input#uap_user_url_field').prop("required",true);
					$('input#uap_socail_account_url_field[type="checkbox"][value="Facebook"]').attr('disabled', false);
					$('input#uap_socail_account_url_field[type="checkbox"][value="Facebook"]').attr('checked', false);

					$('input#uap_socail_account_url_fb_field').prop("required",false);
					$('input#uap_socail_account_url_insta_field').prop("required",false);
				}
			});
				
		
		$('form#uap_createuser input#uap_socail_account_url_fb_field').parent().css("position", "unset");
		$('form#uap_createuser input#uap_socail_account_url_insta_field').parent().css("position", "unset");

		$('form#uap_createuser input#uap_socail_account_url_tiktok_field').parent().css("position", "unset");
		$('form#uap_createuser input#uap_county_uk_only_field').parent().css("position", "unset");
		

		$('form#uap_edituser input#uap_user_url_field').parent().css("position", "unset");
		$('form#uap_edituser input#uap_addr1_field').parent().css("position", "unset");
	});	
	
	/* jQuery(document).ready(function($){
		$('form.checkout').find('div.woocommerce-account-fields p input#createaccount').each(function(){
			if(this.checked == true){
			$('form.checkkout a#crt_woo_usr').click(function(){
				$('form.checkkout input#createaccount').attr("checked",false);
					//$('input#createaccount').prop("checked",false);
					//$('input#createaccount').prop("value",0);
				});
			
			}
			else{
				console.log("error");
			}
	 	}); 
	 });  */
  
});

